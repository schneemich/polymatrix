import dataclassabc

from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.polymatrix.mixins import BroadcastPolyMatrixMixin
from polymatrix.polymatrix.typing import PolynomialData


@dataclassabc.dataclassabc(frozen=True)
class PolyMatrixImpl(PolyMatrix):
    data: dict[tuple[int, int], PolynomialData]
    shape: tuple[int, int]


@dataclassabc.dataclassabc(frozen=True)
class BroadcastPolyMatrixImpl(BroadcastPolyMatrixMixin):
    data: tuple[tuple[int], float]
    shape: tuple[int, int]
