from polymatrix.polymatrix.typing import MonomialData


def sort_monomials(
    monomials: tuple[MonomialData],
) -> tuple[MonomialData]:
    return tuple(
        sorted(
            monomials,
            key=lambda m: (sum(count for _, count in m), len(m), m),
        )
    )
