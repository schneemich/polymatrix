from polymatrix.polymatrix.typing import MonomialData

from polymatrix.polymatrix.utils.sortmonomialindices import sort_monomial_indices


class SubtractError(Exception):
    pass


def subtract_monomial_indices(
    m1: MonomialData,
    m2: MonomialData,
) -> MonomialData:
    m1_dict = dict(m1)

    for index, count in m2:
        if index not in m1_dict:
            raise SubtractError()

        m1_dict[index] -= count

        if m1_dict[index] == 0:
            del m1_dict[index]

        elif m1_dict[index] < 0:
            raise SubtractError()

    return sort_monomial_indices(m1_dict.items())
