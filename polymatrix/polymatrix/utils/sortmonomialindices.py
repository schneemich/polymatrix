from polymatrix.polymatrix.typing import MonomialData


def sort_monomial_indices(
    monomial: MonomialData,
) -> MonomialData:
    return tuple(
        sorted(
            monomial,
            key=lambda m: m[0],
        )
    )
