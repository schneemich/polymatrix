from polymatrix.polymatrix.typing import MonomialData
from polymatrix.polymatrix.utils.sortmonomialindices import sort_monomial_indices


def merge_monomial_indices(
    monomials: tuple[MonomialData, ...],
) -> MonomialData:
    """
    (x1**2 x2, x2**2)  ->  x1**2 x2**3

    or in terms of indices {x1: 0, x2: 1}:

        (
            ((0, 2), (1, 1)),      # x1**2 x2
            ((1, 2),)              # x2**2
        )  ->  ((0, 2), (1, 3))    # x1**1 x2**3
    """

    if len(monomials) == 0:
        return tuple()

    elif len(monomials) == 1:
        return monomials[0]

    else:
        m1_dict = dict(monomials[0])

        for other in monomials[1:]:
            for index, count in other:
                if index in m1_dict:
                    m1_dict[index] += count
                else:
                    m1_dict[index] = count

        # sort monomials according to their index
        # ((1, 3), (0, 2))  ->  ((0, 2), (1, 3))
        return sort_monomial_indices(m1_dict.items())
