import itertools
import math

from polymatrix.polymatrix.utils.mergemonomialindices import merge_monomial_indices
from polymatrix.polymatrix.typing import PolynomialData


def multiply_polynomial(
    left: PolynomialData,
    right: PolynomialData,
    result: PolynomialData,
) -> None:
    """
    Multiplies two polynomials `left` and `right` and adds the result to the mutable polynomial `result`.
    """

    for (left_monomial, left_value), (right_monomial, right_value) in itertools.product(
        left.items(), right.items()
    ):
        value = left_value * right_value

        if math.isclose(value, 0, abs_tol=1e-12):
            continue

        monomial = merge_monomial_indices((left_monomial, right_monomial))

        if monomial not in result:
            result[monomial] = 0

        result[monomial] += value

        if math.isclose(result[monomial], 0, abs_tol=1e-12):
            del result[monomial]
