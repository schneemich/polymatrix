from polymatrix.polymatrix.typing import MonomialData


def split_monomial_indices(
    monomial: MonomialData,
) -> tuple[MonomialData, MonomialData]:
    left = []
    right = []

    is_left = True

    for idx, count in monomial:
        count_left = count // 2

        if count % 2:
            if is_left:
                count_left = count_left + 1

            is_left = not is_left

        count_right = count - count_left

        if 0 < count_left:
            left.append((idx, count_left))

        if 0 < count_right:
            right.append((idx, count - count_left))

    return tuple(left), tuple(right)
