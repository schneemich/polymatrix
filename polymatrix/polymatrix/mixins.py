import abc
import typing

from polymatrix.polymatrix.typing import PolynomialData


class PolyMatrixMixin(abc.ABC):
    @property
    @abc.abstractclassmethod
    def shape(self) -> tuple[int, int]: ...

    def gen_data(
        self,
    ) -> typing.Generator[tuple[tuple[int, int], PolynomialData], None, None]:
        for row in range(self.shape[0]):
            for col in range(self.shape[1]):
                polynomial = self.get_poly(row, col)
                if polynomial is None:
                    continue

                yield (row, col), polynomial

    @abc.abstractclassmethod
    def get_poly(self, row: int, col: int) -> PolynomialData | None: ...


class PolyMatrixAsDictMixin(
    PolyMatrixMixin,
    abc.ABC,
):
    @property
    @abc.abstractmethod
    def data(self) -> dict[tuple[int, int], PolynomialData]: ...

    # overwrites the abstract method of `PolyMatrixMixin`
    def get_poly(self, row: int, col: int) -> PolynomialData | None:
        if (row, col) in self.data:
            return self.data[row, col]


class BroadcastPolyMatrixMixin(
    PolyMatrixMixin,
    abc.ABC,
):
    @property
    @abc.abstractmethod
    def data(self) -> PolynomialData: ...

    # overwrites the abstract method of `PolyMatrixMixin`
    def get_poly(self, col: int, row: int) -> PolynomialData | None:
        return self.data
