from polymatrix.polymatrix.impl import BroadcastPolyMatrixImpl, PolyMatrixImpl
from polymatrix.polymatrix.typing import PolynomialData


def init_poly_matrix(
    data: dict[tuple[int, int], PolynomialData],
    shape: tuple[int, int],
):
    return PolyMatrixImpl(
        data=data,
        shape=shape,
    )


def init_broadcast_poly_matrix(
    data: PolynomialData,
    shape: tuple[int, int],
):
    return BroadcastPolyMatrixImpl(
        data=data,
        shape=shape,
    )
