import abc

from polymatrix.polymatrix.mixins import PolyMatrixAsDictMixin


class PolyMatrix(PolyMatrixAsDictMixin, abc.ABC):
    pass
