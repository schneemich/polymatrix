# monomial x1**2 x2
# with indices {x1: 0, x2: 1}
# is represented as ((0, 2), (1, 1))
MonomialData = tuple[tuple[int, int], ...]

PolynomialData = dict[MonomialData, int | float]
PolynomialMatrixData = dict[tuple[int, int], PolynomialData]
