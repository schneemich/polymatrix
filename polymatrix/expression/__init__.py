import polymatrix.expression.from_
import polymatrix.expression.impl

from collections.abc import Iterable

from polymatrix.utils.getstacklines import get_stack_lines
from polymatrix.expression.expression import init_expression, Expression


def v_stack(
    expressions: Iterable[Expression],
) -> Expression:
    def gen_underlying():
        for expr in expressions:
            if isinstance(expr, Expression):
                yield expr
            else:
                yield polymatrix.expression.from_.from_(expr)

    return init_expression(
        underlying=polymatrix.expression.impl.VStackExprImpl(
            underlying=tuple(gen_underlying()),
        ),
    )


def h_stack(
    expressions: Iterable[Expression],
) -> Expression:
    return v_stack((expr.T for expr in expressions)).T


def block_diag(
    expressions: tuple[Expression],
) -> Expression:
    return init_expression(
        underlying=polymatrix.expression.impl.BlockDiagExprImpl(
            underlying=expressions,
        )
    )


def product(
    expressions: Iterable[Expression],
    degrees: tuple[int, ...] = None,
):
    return init_expression(
        underlying=polymatrix.expression.impl.ProductExprImpl(
            underlying=tuple(expressions),
            degrees=degrees,
            stack=get_stack_lines(),
        )
    )
