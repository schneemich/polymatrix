import abc
import dataclasses
import dataclassabc
import typing
import numpy as np

import polymatrix.expression.init

from polymatrix.utils.getstacklines import get_stack_lines
from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.expressionstate.abc import ExpressionState
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expression.op import (
    diff,
    integrate,
    linear_in,
    linear_monomials,
    legendre,
    filter_,
    degree,
)


class Expression(
    ExpressionBaseMixin,
    abc.ABC,
):
    @property
    @abc.abstractmethod
    def underlying(self) -> ExpressionBaseMixin: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(self, state: ExpressionState) -> tuple[ExpressionState, PolyMatrix]:
        return self.underlying.apply(state)

    def read(self, state: ExpressionState) -> PolyMatrix:
        return self.apply(state)[1]

    def __add__(self, other: ExpressionBaseMixin) -> "Expression":
        return self._binary(polymatrix.expression.init.init_addition_expr, self, other)

    def __getattr__(self, name):
        attr = getattr(self.underlying, name)

        if isinstance(attr, ExpressionBaseMixin):
            return dataclasses.replace(
                self,
                underlying=attr,
            )

        else:
            return attr

    def __getitem__(self, key: tuple[int, int]):
        return self.copy(
            underlying=polymatrix.expression.init.init_get_item_expr(
                underlying=self.underlying,
                index=key,
            ),
        )

    def __matmul__(
        self, other: typing.Union[ExpressionBaseMixin, np.ndarray]
    ) -> "Expression":
        return self._binary(
            polymatrix.expression.init.init_matrix_mult_expr, self, other
        )

    def __mul__(self, other) -> "Expression":
        return self._binary(polymatrix.expression.init.init_elem_mult_expr, self, other)

    def __pow__(self, num):
        curr = 1

        for _ in range(num):
            curr = curr * self

        return curr

    def __neg__(self):
        return self * (-1)

    def __radd__(self, other):
        return self._binary(polymatrix.expression.init.init_addition_expr, other, self)

    def __rmatmul__(self, other):
        return self._binary(
            polymatrix.expression.init.init_matrix_mult_expr, other, self
        )

    def __rmul__(self, other):
        return self * other

    def __rsub__(self, other):
        return other + (-self)

    def __sub__(self, other):
        return self + (-other)

    def __truediv__(self, other: ExpressionBaseMixin):
        return self._binary(polymatrix.expression.init.init_division_expr, self, other)

    @abc.abstractmethod
    def copy(self, underlying: ExpressionBaseMixin) -> "Expression": ...

    @staticmethod
    def _binary(op, left, right):
        stack = get_stack_lines()

        if isinstance(left, Expression):
            right = polymatrix.expression.init.init_from_expr_or_none(right)

            # delegate to upper level
            if right is None:
                return NotImplemented

            return left.copy(
                underlying=op(left, right, stack),
            )

        else:
            left = polymatrix.expression.init.init_from_expr_or_none(left)

            # delegate to upper level
            if left is None:
                return NotImplemented

            return right.copy(
                underlying=op(left, right, stack),
            )

    def cache(self) -> "Expression":
        return self.copy(
            underlying=polymatrix.expression.init.init_cache_expr(
                underlying=self.underlying,
            ),
        )

    def combinations(
        self,
        degrees: tuple[int, ...] | int,
    ):
        return self.copy(
            underlying=polymatrix.expression.init.init_combinations_expr(
                expression=self.underlying,
                degrees=degrees,
            ),
        )

    def degree(self) -> "Expression":
        return self.copy(
            underlying=degree(
                underlying=self.underlying,
            ),
        )

    def determinant(self) -> "Expression":
        return self.copy(
            underlying=polymatrix.expression.init.init_determinant_expr(
                underlying=self.underlying,
            ),
        )

    def diag(self):
        return self.copy(
            underlying=polymatrix.expression.init.init_diag_expr(
                underlying=self.underlying,
            ),
        )

    def diff(
        self,
        variables: "Expression",
        introduce_derivatives: bool = None,
    ) -> "Expression":
        return self.copy(
            underlying=diff(
                expression=self,
                variables=variables,
                introduce_derivatives=introduce_derivatives,
            ),
        )

    def divergence(
        self,
        variables: tuple,
    ) -> "Expression":
        return self.copy(
            underlying=polymatrix.expression.init.init_divergence_expr(
                underlying=self.underlying,
                variables=variables,
            ),
        )

    def eval(
        self,
        variable: tuple,
        value: tuple[float, ...] = None,
    ) -> "Expression":
        return self.copy(
            underlying=polymatrix.expression.init.init_eval_expr(
                underlying=self.underlying,
                variables=variable,
                values=value,
            ),
        )

    # also applies to monomials (and variables?)
    def filter(
        self,
        predicator: "Expression",
        inverse: bool = None,
    ) -> "Expression":
        return self.copy(
            underlying=filter_(
                underlying=self.underlying,
                predicator=predicator,
                inverse=inverse,
            ),
        )

    # only applies to symmetric matrix
    def from_symmetric_matrix(self) -> "Expression":
        return self.copy(
            underlying=polymatrix.expression.init.init_from_symmetric_matrix_expr(
                underlying=self.underlying,
            ),
        )

    # only applies to monomials
    def half_newton_polytope(
        self,
        variables: "Expression",
        filter: "Expression | None" = None,
    ) -> "Expression":
        return self.copy(
            underlying=polymatrix.expression.init.init_half_newton_polytope_expr(
                monomials=self.underlying,
                variables=variables,
                filter=filter,
            ),
        )

    def integrate(
        self, variables: "Expression", from_: tuple[float, ...], to: tuple[float, ...]
    ) -> "Expression":
        return self.copy(
            underlying=integrate(
                expression=self,
                variables=variables,
                from_=from_,
                to=to,
            ),
        )

    def linear_matrix_in(self, variable: "Expression") -> "Expression":
        return self.copy(
            underlying=polymatrix.expression.init.init_linear_matrix_in_expr(
                underlying=self.underlying,
                variable=variable,
            ),
        )

    def linear_monomials(
        self,
        variables: "Expression",
    ) -> "Expression":
        return self.copy(
            underlying=linear_monomials(
                expression=self.underlying,
                variables=variables,
            ),
        )

    def linear_in(
        self,
        variables: "Expression",
        monomials: "Expression" = None,
        ignore_unmatched: bool = None,
    ) -> "Expression":
        return self.copy(
            underlying=linear_in(
                expression=self.underlying,
                monomials=monomials,
                variables=variables,
                ignore_unmatched=ignore_unmatched,
            ),
        )

    def legendre(
        self,
        degrees: tuple[int, ...] = None,
    ) -> "Expression":
        return self.copy(
            underlying=legendre(
                expression=self.underlying,
                degrees=degrees,
            ),
        )

    def max(self) -> "Expression":
        return self.copy(
            underlying=polymatrix.expression.init.init_max_expr(
                underlying=self.underlying,
            ),
        )

    def parametrize(self, name: str = None) -> "Expression":
        return self.copy(
            underlying=polymatrix.expression.init.init_parametrize_expr(
                underlying=self.underlying,
                name=name,
            ),
        )

    def quadratic_in(
        self, variables: "Expression", monomials: "Expression" = None
    ) -> "Expression":
        if monomials is None:
            monomials = self.quadratic_monomials(variables)

        stack = get_stack_lines()

        return self.copy(
            underlying=polymatrix.expression.init.init_quadratic_in_expr(
                underlying=self.underlying,
                monomials=monomials,
                variables=variables,
                stack=stack,
            ),
        )

    def quadratic_monomials(
        self,
        variables: "Expression",
    ) -> "Expression":
        return self.copy(
            underlying=polymatrix.expression.init.init_quadratic_monomials_expr(
                underlying=self.underlying,
                variables=variables,
            ),
        )

    def reshape(self, n: int, m: int) -> "Expression":
        return self.copy(
            underlying=polymatrix.expression.init.init_reshape_expr(
                underlying=self.underlying,
                new_shape=(n, m),
            ),
        )

    def rep_mat(self, n: int, m: int) -> "Expression":
        return self.copy(
            underlying=polymatrix.expression.init.init_rep_mat_expr(
                underlying=self.underlying,
                repetition=(n, m),
            ),
        )

    def set_element_at(
        self,
        row: int,
        col: int,
        value: "Expression",
    ) -> "Expression":
        if isinstance(value, Expression):
            value = value.underlying
        else:
            value = polymatrix.expression.init.init_from_expr(value)

        return self.copy(
            underlying=polymatrix.expression.init.init_set_element_at_expr(
                underlying=self.underlying,
                index=(row, col),
                value=value,
            ),
        )

    # remove?
    def squeeze(
        self,
    ) -> "Expression":
        return self.copy(
            underlying=polymatrix.expression.init.init_squeeze_expr(
                underlying=self.underlying,
            ),
        )

    # only applies to monomials
    def subtract_monomials(
        self,
        monomials: "Expression",
    ) -> "Expression":
        return self.copy(
            underlying=polymatrix.expression.init.init_subtract_monomials_expr(
                underlying=self.underlying,
                monomials=monomials,
            ),
        )

    def substitute(
        self,
        variable: tuple,
        values: tuple["Expression", ...] = None,
    ) -> "Expression":
        return self.copy(
            underlying=polymatrix.expression.init.init_substitute_expr(
                underlying=self.underlying,
                variables=variable,
                values=values,
            ),
        )

    def subs(
        self,
        variable: tuple,
        values: tuple["Expression", ...] = None,
    ) -> "Expression":
        return self.substitute(
            variable=variable,
            values=values,
        )

    def sum(self):
        return self.copy(
            underlying=polymatrix.expression.init.init_sum_expr(
                underlying=self.underlying,
            ),
        )

    def symmetric(self):
        return self.copy(
            underlying=polymatrix.expression.init.init_symmetric_expr(
                underlying=self.underlying,
            ),
        )

    def transpose(self) -> "Expression":
        return self.copy(
            underlying=polymatrix.expression.init.init_transpose_expr(
                underlying=self.underlying,
            ),
        )

    @property
    def T(self) -> "Expression":
        return self.transpose()

    def to_constant(self) -> "Expression":
        return self.copy(
            underlying=polymatrix.expression.init.init_to_constant_expr(
                underlying=self.underlying,
            ),
        )

    def to_symmetric_matrix(self) -> "Expression":
        return self.copy(
            underlying=polymatrix.expression.init.init_to_symmetric_matrix_expr(
                underlying=self.underlying,
            ),
        )

    # only applies to variables
    def to_sorted_variables(self) -> "Expression":
        return self.copy(
            underlying=polymatrix.expression.init.init_to_sorted_variables(
                underlying=self.underlying,
            ),
        )

    # also applies to monomials?
    def truncate(
        self,
        degrees: tuple[int],
        variables: tuple | None = None,
        inverse: bool = None,
    ):
        return self.copy(
            underlying=polymatrix.expression.init.init_truncate_expr(
                underlying=self.underlying,
                variables=variables,
                degrees=degrees,
                inverse=inverse,
            ),
        )


@dataclassabc.dataclassabc(frozen=True, repr=False)
class ExpressionImpl(Expression):
    underlying: ExpressionBaseMixin

    def __repr__(self) -> str:
        return self.underlying.__repr__()

    def copy(self, underlying: ExpressionBaseMixin) -> "Expression":
        return dataclasses.replace(
            self,
            underlying=underlying,
        )


def init_expression(
    underlying: ExpressionBaseMixin,
):
    return ExpressionImpl(
        underlying=underlying,
    )
