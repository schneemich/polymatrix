import typing
import dataclassabc
from polymatrix.expression.mixins.integrateexprmixin import IntegrateExprMixin

from polymatrix.expression.mixins.legendreseriesmixin import LegendreSeriesMixin
from polymatrix.expression.mixins.productexprmixin import ProductExprMixin
from polymatrix.utils.getstacklines import FrameSummary
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expression.mixins.additionexprmixin import AdditionExprMixin
from polymatrix.expression.mixins.blockdiagexprmixin import BlockDiagExprMixin
from polymatrix.expression.mixins.cacheexprmixin import CacheExprMixin
from polymatrix.expression.mixins.combinationsexprmixin import CombinationsExprMixin
from polymatrix.expression.mixins.derivativeexprmixin import DerivativeExprMixin
from polymatrix.expression.mixins.determinantexprmixin import DeterminantExprMixin
from polymatrix.expression.mixins.diagexprmixin import DiagExprMixin
from polymatrix.expression.mixins.divergenceexprmixin import DivergenceExprMixin
from polymatrix.expression.mixins.divisionexprmixin import DivisionExprMixin
from polymatrix.expression.mixins.elemmultexprmixin import ElemMultExprMixin
from polymatrix.expression.mixins.evalexprmixin import EvalExprMixin
from polymatrix.expression.mixins.eyeexprmixin import EyeExprMixin
from polymatrix.expression.mixins.filterexprmixin import FilterExprMixin
from polymatrix.expression.mixins.fromsymmetricmatrixexprmixin import (
    FromSymmetricMatrixExprMixin,
)
from polymatrix.expression.mixins.fromtupleexprmixin import FromTupleExprMixin
from polymatrix.expression.mixins.fromtermsexprmixin import (
    FromPolynomialDataExprMixin,
    PolynomialMatrixTupledData,
)
from polymatrix.expression.mixins.getitemexprmixin import GetItemExprMixin
from polymatrix.expression.mixins.halfnewtonpolytopeexprmixin import (
    HalfNewtonPolytopeExprMixin,
)
from polymatrix.expression.mixins.linearinexprmixin import LinearInExprMixin
from polymatrix.expression.mixins.linearmatrixinexprmixin import LinearMatrixInExprMixin
from polymatrix.expression.mixins.linearmonomialsexprmixin import (
    LinearMonomialsExprMixin,
)
from polymatrix.expression.mixins.matrixmultexprmixin import MatrixMultExprMixin
from polymatrix.expression.mixins.degreeexprmixin import DegreeExprMixin
from polymatrix.expression.mixins.maxexprmixin import MaxExprMixin
from polymatrix.expression.mixins.parametrizeexprmixin import ParametrizeExprMixin
from polymatrix.expression.mixins.parametrizematrixexprmixin import (
    ParametrizeMatrixExprMixin,
)
from polymatrix.expression.mixins.quadraticinexprmixin import QuadraticInExprMixin
from polymatrix.expression.mixins.quadraticmonomialsexprmixin import (
    QuadraticMonomialsExprMixin,
)
from polymatrix.expression.mixins.repmatexprmixin import RepMatExprMixin
from polymatrix.expression.mixins.reshapeexprmixin import ReshapeExprMixin
from polymatrix.expression.mixins.setelementatexprmixin import SetElementAtExprMixin
from polymatrix.expression.mixins.squeezeexprmixin import SqueezeExprMixin
from polymatrix.expression.mixins.substituteexprmixin import SubstituteExprMixin
from polymatrix.expression.mixins.subtractmonomialsexprmixin import (
    SubtractMonomialsExprMixin,
)
from polymatrix.expression.mixins.sumexprmixin import SumExprMixin
from polymatrix.expression.mixins.symmetricexprmixin import SymmetricExprMixin
from polymatrix.expression.mixins.toconstantexprmixin import ToConstantExprMixin
from polymatrix.expression.mixins.toquadraticexprmixin import ToQuadraticExprMixin
from polymatrix.expression.mixins.tosymmetricmatrixexprmixin import (
    ToSymmetricMatrixExprMixin,
)
from polymatrix.expression.mixins.transposeexprmixin import TransposeExprMixin
from polymatrix.expression.mixins.truncateexprmixin import TruncateExprMixin
from polymatrix.expression.mixins.vstackexprmixin import VStackExprMixin
from polymatrix.expression.mixins.tosortedvariablesmixin import (
    ToSortedVariablesExprMixin,
)


@dataclassabc.dataclassabc(frozen=True)
class AdditionExprImpl(AdditionExprMixin):
    left: ExpressionBaseMixin
    right: ExpressionBaseMixin
    stack: tuple[FrameSummary]

    # implement custom __repr__ method that returns a representation without the stack
    def __repr__(self):
        return f"{self.__class__.__name__}(left={self.left}, right={self.right})"


@dataclassabc.dataclassabc(frozen=True)
class BlockDiagExprImpl(BlockDiagExprMixin):
    underlying: tuple[ExpressionBaseMixin]


@dataclassabc.dataclassabc(frozen=True)
class CacheExprImpl(CacheExprMixin):
    underlying: ExpressionBaseMixin


@dataclassabc.dataclassabc(frozen=True)
class CombinationsExprImpl(CombinationsExprMixin):
    expression: ExpressionBaseMixin
    degrees: tuple[int, ...]


@dataclassabc.dataclassabc(frozen=True)
class DerivativeExprImpl(DerivativeExprMixin):
    underlying: ExpressionBaseMixin
    variables: tuple
    introduce_derivatives: bool
    stack: tuple[FrameSummary]

    # implement custom __repr__ method that returns a representation without the stack
    def __repr__(self):
        return f"{self.__class__.__name__}(variables={self.variables}, underlying={self.underlying})"


@dataclassabc.dataclassabc(frozen=True)
class DeterminantExprImpl(DeterminantExprMixin):
    underlying: ExpressionBaseMixin


@dataclassabc.dataclassabc(frozen=True)
class DiagExprImpl(DiagExprMixin):
    underlying: ExpressionBaseMixin


@dataclassabc.dataclassabc(frozen=True)
class DivergenceExprImpl(DivergenceExprMixin):
    underlying: ExpressionBaseMixin
    variables: tuple


@dataclassabc.dataclassabc(frozen=True)
class DivisionExprImpl(DivisionExprMixin):
    left: ExpressionBaseMixin
    right: ExpressionBaseMixin
    stack: tuple[FrameSummary]

    # implement custom __repr__ method that returns a representation without the stack
    def __repr__(self):
        return f"{self.__class__.__name__}(left={self.left}, right={self.right})"


@dataclassabc.dataclassabc(frozen=True)
class ElemMultExprImpl(ElemMultExprMixin):
    left: ExpressionBaseMixin
    right: ExpressionBaseMixin


@dataclassabc.dataclassabc(frozen=True)
class EvalExprImpl(EvalExprMixin):
    underlying: ExpressionBaseMixin
    substitutions: tuple


@dataclassabc.dataclassabc(frozen=True)
class EyeExprImpl(EyeExprMixin):
    variable: ExpressionBaseMixin


@dataclassabc.dataclassabc(frozen=True)
class FilterExprImpl(FilterExprMixin):
    underlying: ExpressionBaseMixin
    predicator: ExpressionBaseMixin
    inverse: bool


@dataclassabc.dataclassabc(frozen=True)
class FromSymmetricMatrixExprImpl(FromSymmetricMatrixExprMixin):
    underlying: ExpressionBaseMixin


@dataclassabc.dataclassabc(frozen=True)
class FromTupleExprImpl(FromTupleExprMixin):
    data: tuple[tuple[float]]
    stack: tuple[FrameSummary]

    # implement custom __repr__ method that returns a representation without the stack
    def __repr__(self):
        return f"{self.__class__.__name__}(data={self.data})"


@dataclassabc.dataclassabc(frozen=True)
class FromPolynomialDataExprImpl(FromPolynomialDataExprMixin):
    data: PolynomialMatrixTupledData
    shape: tuple[int, int]


@dataclassabc.dataclassabc(frozen=True)
class GetItemExprImpl(GetItemExprMixin):
    underlying: ExpressionBaseMixin
    index: tuple[tuple[int, ...], tuple[int, ...]]


@dataclassabc.dataclassabc(frozen=True)
class HalfNewtonPolytopeExprImpl(HalfNewtonPolytopeExprMixin):
    monomials: ExpressionBaseMixin
    variables: ExpressionBaseMixin
    filter: ExpressionBaseMixin | None


@dataclassabc.dataclassabc(frozen=True)
class IntegrateExprImpl(IntegrateExprMixin):
    underlying: ExpressionBaseMixin
    variables: ExpressionBaseMixin
    from_: tuple[float, ...]
    to: tuple[float, ...]
    stack: tuple[FrameSummary]

    # implement custom __repr__ method that returns a representation without the stack
    def __repr__(self):
        return f"{self.__class__.__name__}(underlying={self.underlying}, variables={self.variables}, from_={self.from_}, to={self.to})"


@dataclassabc.dataclassabc(frozen=True)
class LinearInExprImpl(LinearInExprMixin):
    underlying: ExpressionBaseMixin
    monomials: ExpressionBaseMixin
    variables: ExpressionBaseMixin
    ignore_unmatched: bool


@dataclassabc.dataclassabc(frozen=True)
class LinearMatrixInExprImpl(LinearMatrixInExprMixin):
    underlying: ExpressionBaseMixin
    variable: int


@dataclassabc.dataclassabc(frozen=True)
class LinearMonomialsExprImpl(LinearMonomialsExprMixin):
    underlying: ExpressionBaseMixin
    variables: tuple


@dataclassabc.dataclassabc(frozen=True)
class LegendreSeriesImpl(LegendreSeriesMixin):
    underlying: ExpressionBaseMixin
    degrees: tuple[int, ...] | None
    stack: tuple[FrameSummary]

    # implement custom __repr__ method that returns a representation without the stack
    def __repr__(self):
        return f"{self.__class__.__name__}(underlying={self.underlying}, degrees={self.degrees})"


@dataclassabc.dataclassabc(frozen=True)
class MatrixMultExprImpl(MatrixMultExprMixin):
    left: ExpressionBaseMixin
    right: ExpressionBaseMixin
    stack: tuple[FrameSummary]

    # implement custom __repr__ method that returns a representation without the stack
    def __repr__(self):
        return f"{self.__class__.__name__}(left={self.left}, right={self.right})"


@dataclassabc.dataclassabc(frozen=True)
class DegreeExprImpl(DegreeExprMixin):
    underlying: ExpressionBaseMixin
    stack: tuple[FrameSummary]

    # implement custom __repr__ method that returns a representation without the stack
    def __repr__(self):
        return f"{self.__class__.__name__}(underlying={self.underlying})"


@dataclassabc.dataclassabc(frozen=True)
class MaxExprImpl(MaxExprMixin):
    underlying: ExpressionBaseMixin


@dataclassabc.dataclassabc(frozen=True, repr=False)
class ParametrizeExprImpl(ParametrizeExprMixin):
    underlying: ExpressionBaseMixin
    name: str

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}(name={self.name}, underlying={self.underlying})"
        )


@dataclassabc.dataclassabc(frozen=True)
class ParametrizeMatrixExprImpl(ParametrizeMatrixExprMixin):
    underlying: ExpressionBaseMixin
    name: str


@dataclassabc.dataclassabc(frozen=True)
class ProductExprImpl(ProductExprMixin):
    underlying: tuple[ExpressionBaseMixin]
    degrees: tuple[int, ...] | None
    stack: tuple[FrameSummary]

    # implement custom __repr__ method that returns a representation without the stack
    def __repr__(self):
        return f"{self.__class__.__name__}(underlying={self.underlying}, degrees={self.degrees})"


@dataclassabc.dataclassabc(frozen=True)
class QuadraticInExprImpl(QuadraticInExprMixin):
    underlying: ExpressionBaseMixin
    monomials: ExpressionBaseMixin
    variables: tuple
    stack: tuple[FrameSummary]

    # implement custom __repr__ method that returns a representation without the stack
    def __repr__(self):
        return f"{self.__class__.__name__}(variables={self.variables}, monomials={self.monomials}, underlying={self.underlying})"


@dataclassabc.dataclassabc(frozen=True)
class QuadraticMonomialsExprImpl(QuadraticMonomialsExprMixin):
    underlying: ExpressionBaseMixin
    variables: tuple


@dataclassabc.dataclassabc(frozen=True)
class RepMatExprImpl(RepMatExprMixin):
    underlying: ExpressionBaseMixin
    repetition: tuple


@dataclassabc.dataclassabc(frozen=True)
class ReshapeExprImpl(ReshapeExprMixin):
    underlying: ExpressionBaseMixin
    new_shape: tuple


@dataclassabc.dataclassabc(frozen=True)
class SetElementAtExprImpl(SetElementAtExprMixin):
    underlying: ExpressionBaseMixin
    index: tuple
    value: ExpressionBaseMixin


@dataclassabc.dataclassabc(frozen=True)
class SqueezeExprImpl(SqueezeExprMixin):
    underlying: ExpressionBaseMixin


@dataclassabc.dataclassabc(frozen=True)
class SubstituteExprImpl(SubstituteExprMixin):
    underlying: ExpressionBaseMixin
    substitutions: tuple


@dataclassabc.dataclassabc(frozen=True)
class SubtractMonomialsExprImpl(SubtractMonomialsExprMixin):
    underlying: ExpressionBaseMixin
    monomials: ExpressionBaseMixin


@dataclassabc.dataclassabc(frozen=True)
class SumExprImpl(SumExprMixin):
    underlying: ExpressionBaseMixin


@dataclassabc.dataclassabc(frozen=True)
class SymmetricExprImpl(SymmetricExprMixin):
    underlying: ExpressionBaseMixin


@dataclassabc.dataclassabc(frozen=True)
class ToConstantExprImpl(ToConstantExprMixin):
    underlying: ExpressionBaseMixin


@dataclassabc.dataclassabc(frozen=True)
class ToQuadraticExprImpl(ToQuadraticExprMixin):
    underlying: ExpressionBaseMixin


@dataclassabc.dataclassabc(frozen=True)
class ToSortedVariablesImpl(ToSortedVariablesExprMixin):
    underlying: ExpressionBaseMixin


@dataclassabc.dataclassabc(frozen=True)
class ToSymmetricMatrixExprImpl(ToSymmetricMatrixExprMixin):
    underlying: ExpressionBaseMixin


@dataclassabc.dataclassabc(frozen=True)
class TransposeExprImpl(TransposeExprMixin):
    underlying: ExpressionBaseMixin


@dataclassabc.dataclassabc(frozen=True)
class TruncateExprImpl(TruncateExprMixin):
    underlying: ExpressionBaseMixin
    variables: ExpressionBaseMixin
    degrees: tuple[int]
    inverse: bool


@dataclassabc.dataclassabc(frozen=True)
class VStackExprImpl(VStackExprMixin):
    underlying: tuple
