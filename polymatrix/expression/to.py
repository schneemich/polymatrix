import sympy
import numpy as np

from polymatrix.expression.expression import Expression
from polymatrix.expression.mixins.parametrizeexprmixin import ParametrizeExprMixin
from polymatrix.expression.mixins.parametrizematrixexprmixin import (
    ParametrizeMatrixExprMixin,
)
from polymatrix.expressionstate.abc import ExpressionState
from polymatrix.statemonad.init import init_state_monad
from polymatrix.statemonad.mixins import StateMonadMixin


def shape(
    expr: Expression,
) -> StateMonadMixin[ExpressionState, tuple[int, ...]]:
    def func(state: ExpressionState):
        state, polymatrix = expr.apply(state)

        return state, polymatrix.shape

    return init_state_monad(func)


def to_constant(
    expr: Expression,
    assert_constant: bool = True,
) -> StateMonadMixin[ExpressionState, np.ndarray]:
    def func(state: ExpressionState):
        state, underlying = expr.apply(state)

        A = np.zeros(underlying.shape, dtype=np.double)

        for (row, col), polynomial in underlying.gen_data():
            for monomial, value in polynomial.items():
                if len(monomial) == 0:
                    A[row, col] = value

                elif assert_constant:
                    raise Exception(f"non-constant term {monomial=}")

        return state, A

    return init_state_monad(func)


def to_sympy(
    expr: Expression,
) -> StateMonadMixin[ExpressionState, sympy.Expr]:
    def func(state: ExpressionState):
        state, underlying = expr.apply(state)

        A = np.zeros(underlying.shape, dtype=object)

        for (row, col), polynomial in underlying.gen_data():
            sympy_polynomial = 0

            for monomial, value in polynomial.items():
                sympy_monomial = 1

                for offset, count in monomial:
                    variable = state.get_key_from_offset(offset)
                    # def get_variable_from_offset(offset: int):
                    #     for variable, (start, end) in state.offset_dict.items():
                    #         if start <= offset < end:
                    # assert end - start == 1, f'{start=}, {end=}, {variable=}'

                    if isinstance(variable, sympy.core.symbol.Symbol):
                        variable_name = variable.name
                    elif isinstance(
                        variable, (ParametrizeExprMixin, ParametrizeMatrixExprMixin)
                    ):
                        variable_name = variable.name
                    elif isinstance(variable, str):
                        variable_name = variable
                    else:
                        raise Exception(f"{variable=}")

                    start, end = state.offset_dict[variable]

                    if end - start == 1:
                        sympy_var = sympy.Symbol(variable_name)
                    else:
                        sympy_var = sympy.Symbol(
                            f"{variable_name}_{offset - start + 1}"
                        )

                    # var = get_variable_from_offset(offset)
                    sympy_monomial *= sympy_var**count

                sympy_polynomial += value * sympy_monomial

            A[row, col] = sympy_polynomial

        return state, A

    return init_state_monad(func)
