import typing
import numpy as np
import sympy

import polymatrix.expression.impl

from polymatrix.polymatrix.mixins import PolyMatrixMixin
from polymatrix.polymatrix.typing import PolynomialMatrixData
from polymatrix.statemonad.abc import StateMonad
from polymatrix.utils.getstacklines import FrameSummary
from polymatrix.utils.getstacklines import get_stack_lines
from polymatrix.expression.utils.formatsubstitutions import format_substitutions
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expression.impl import FromTupleExprImpl, AdditionExprImpl


def init_addition_expr(
    left: ExpressionBaseMixin,
    right: ExpressionBaseMixin,
    stack: tuple[FrameSummary],
):
    return AdditionExprImpl(
        left=left,
        right=right,
        stack=stack,
    )


def init_block_diag_expr(
    underlying: tuple,
):
    return polymatrix.expression.impl.BlockDiagExprImpl(
        underlying=underlying,
    )


def init_cache_expr(
    underlying: ExpressionBaseMixin,
):
    return polymatrix.expression.impl.CacheExprImpl(
        underlying=underlying,
    )


def init_combinations_expr(
    expression: ExpressionBaseMixin,
    degrees: tuple[int, ...] | int,
):
    if isinstance(degrees, int):
        degrees = (degrees,)

    return polymatrix.expression.impl.CombinationsExprImpl(
        expression=expression,
        degrees=degrees,
    )


def init_determinant_expr(
    underlying: ExpressionBaseMixin,
):
    return polymatrix.expression.impl.DeterminantExprImpl(
        underlying=underlying,
    )


def init_diag_expr(
    underlying: ExpressionBaseMixin,
):
    return polymatrix.expression.impl.DiagExprImpl(
        underlying=underlying,
    )


def init_divergence_expr(
    underlying: ExpressionBaseMixin,
    variables: tuple,
):
    return polymatrix.expression.impl.DivergenceExprImpl(
        underlying=underlying,
        variables=variables,
    )


def init_division_expr(
    left: ExpressionBaseMixin,
    right: ExpressionBaseMixin,
    stack: tuple[FrameSummary],
):
    return polymatrix.expression.impl.DivisionExprImpl(
        left=left,
        right=right,
        stack=stack,
    )


def init_elem_mult_expr(
    left: ExpressionBaseMixin,
    right: ExpressionBaseMixin,
    stack: tuple[FrameSummary],
):
    return polymatrix.expression.impl.ElemMultExprImpl(
        left=left,
        right=right,
    )


def init_eval_expr(
    underlying: ExpressionBaseMixin,
    variables: typing.Union[typing.Any, tuple, dict],
    values: typing.Union[float, tuple] = None,
):
    substitutions = format_substitutions(
        variables=variables,
        values=values,
    )

    def formatted_values(value):
        if isinstance(value, np.ndarray):
            return tuple(value.reshape(-1))

        elif isinstance(value, tuple):
            return value

        elif isinstance(value, int) or isinstance(value, float):
            return (value,)

        else:
            return (float(value),)

    substitutions = tuple(
        (variable, formatted_values(value)) for variable, value in substitutions
    )

    return polymatrix.expression.impl.EvalExprImpl(
        underlying=underlying,
        substitutions=substitutions,
    )


def init_eye_expr(
    variable: ExpressionBaseMixin,
):
    return polymatrix.expression.impl.EyeExprImpl(
        variable=variable,
    )


def init_from_symmetric_matrix_expr(
    underlying: ExpressionBaseMixin,
):
    return polymatrix.expression.impl.FromSymmetricMatrixExprImpl(
        underlying=underlying,
    )


DATA_TYPE = str | np.ndarray | sympy.Matrix | sympy.Expr | tuple | ExpressionBaseMixin


def init_from_expr_or_none(
    data: DATA_TYPE,
) -> ExpressionBaseMixin | None:
    if isinstance(data, str):
        return init_parametrize_expr(
            underlying=init_from_expr_or_none(1),
            name=data,
        )

    elif isinstance(data, StateMonad):
        return data.flat_map(lambda inner_data: init_from_expr_or_none(inner_data))

    elif isinstance(data, np.ndarray):
        assert len(data.shape) <= 2

        def gen_elements():
            for row in data:
                if isinstance(row, np.ndarray):
                    yield tuple(row)
                else:
                    yield (row,)

        data = tuple(gen_elements())

    elif isinstance(data, sympy.Matrix):
        data = tuple(tuple(i for i in data.row(row)) for row in range(data.rows))

    elif isinstance(data, sympy.Expr):
        data = ((sympy.expand(data),),)

    elif isinstance(data, tuple):
        if isinstance(data[0], tuple):
            n_col = len(data[0])
            assert all(len(col) == n_col for col in data)

        else:
            data = tuple((e,) for e in data)

    elif isinstance(data, ExpressionBaseMixin):
        return data

    elif isinstance(data, (float, int, np.number)):
        data = ((data,),)

    else:
        return None

    return FromTupleExprImpl(
        data=data,
        stack=get_stack_lines(),
    )


def init_from_expr(data: DATA_TYPE):
    expr = init_from_expr_or_none(data)

    if expr is None:
        raise Exception(f"{data=}")

    return expr


def init_from_terms_expr(
    data: PolyMatrixMixin | PolynomialMatrixData,
    shape: tuple[int, int] = None,
):
    if isinstance(data, PolyMatrixMixin):
        shape = data.shape
        poly_matrix_data = data.gen_data()

    else:
        assert shape is not None

        if isinstance(data, tuple):
            poly_matrix_data = data

        elif isinstance(data, dict):
            poly_matrix_data = data.items()

        else:
            raise Exception(f"{data=}")

    # Expression needs to be hashable
    data_as_tuple = tuple(
        (coord, tuple(polynomial.items())) for coord, polynomial in poly_matrix_data
    )

    return polymatrix.expression.impl.FromPolynomialDataExprImpl(
        terms=data_as_tuple,
        shape=shape,
    )


def init_get_item_expr(
    underlying: ExpressionBaseMixin,
    index: tuple[tuple[int, ...], tuple[int, ...]],
):
    def get_hashable_slice(index):
        if isinstance(index, slice):
            return polymatrix.expression.impl.GetItemExprImpl.Slice(
                start=index.start, stop=index.stop, step=index.step
            )
        else:
            return index

    proper_index = (get_hashable_slice(index[0]), get_hashable_slice(index[1]))

    return polymatrix.expression.impl.GetItemExprImpl(
        underlying=underlying,
        index=proper_index,
    )


def init_half_newton_polytope_expr(
    monomials: ExpressionBaseMixin,
    variables: ExpressionBaseMixin,
    filter: ExpressionBaseMixin | None = None,
):
    return polymatrix.expression.impl.HalfNewtonPolytopeExprImpl(
        monomials=monomials, variables=variables, filter=filter
    )


def init_linear_matrix_in_expr(
    underlying: ExpressionBaseMixin,
    variable: int,
):
    return polymatrix.expression.impl.LinearMatrixInExprImpl(
        underlying=underlying,
        variable=variable,
    )


def init_matrix_mult_expr(
    left: ExpressionBaseMixin,
    right: ExpressionBaseMixin,
    stack: tuple[FrameSummary],
):
    return polymatrix.expression.impl.MatrixMultExprImpl(
        left=left,
        right=right,
        stack=stack,
    )


def init_max_expr(
    underlying: ExpressionBaseMixin,
):
    return polymatrix.expression.impl.MaxExprImpl(
        underlying=underlying,
    )


def init_parametrize_expr(
    underlying: ExpressionBaseMixin,
    name: str = None,
):
    if name is None:
        name = "undefined"

    return polymatrix.expression.impl.ParametrizeExprImpl(
        underlying=underlying,
        name=name,
    )


def init_parametrize_matrix_expr(
    underlying: ExpressionBaseMixin,
    name: str,
):
    return polymatrix.expression.impl.ParametrizeMatrixExprImpl(
        underlying=underlying,
        name=name,
    )


def init_quadratic_in_expr(
    underlying: ExpressionBaseMixin,
    monomials: ExpressionBaseMixin,
    variables: ExpressionBaseMixin,
    stack: tuple[FrameSummary],
):
    assert isinstance(variables, ExpressionBaseMixin), f"{variables=}"

    return polymatrix.expression.impl.QuadraticInExprImpl(
        underlying=underlying,
        monomials=monomials,
        variables=variables,
        stack=stack,
    )


def init_quadratic_monomials_expr(
    underlying: ExpressionBaseMixin,
    variables: ExpressionBaseMixin,
):
    assert isinstance(variables, ExpressionBaseMixin), f"{variables=}"

    return polymatrix.expression.impl.QuadraticMonomialsExprImpl(
        underlying=underlying,
        variables=variables,
    )


def init_rep_mat_expr(
    underlying: ExpressionBaseMixin,
    repetition: tuple,
):
    return polymatrix.expression.impl.RepMatExprImpl(
        underlying=underlying,
        repetition=repetition,
    )


def init_reshape_expr(
    underlying: ExpressionBaseMixin,
    new_shape: tuple,
):
    return polymatrix.expression.impl.ReshapeExprImpl(
        underlying=underlying,
        new_shape=new_shape,
    )


def init_set_element_at_expr(
    underlying: ExpressionBaseMixin,
    index: tuple,
    value: ExpressionBaseMixin,
):
    return polymatrix.expression.impl.SetElementAtExprImpl(
        underlying=underlying,
        index=index,
        value=value,
    )


def init_squeeze_expr(
    underlying: ExpressionBaseMixin,
):
    return polymatrix.expression.impl.SqueezeExprImpl(
        underlying=underlying,
    )


def init_substitute_expr(
    underlying: ExpressionBaseMixin,
    variables: tuple,
    values: tuple = None,
):
    substitutions = format_substitutions(
        variables=variables,
        values=values,
    )

    def formatted_values(value) -> ExpressionBaseMixin:
        if isinstance(value, ExpressionBaseMixin):
            expr = value

        else:
            expr = init_from_expr(value)

        return polymatrix.expression.impl.ReshapeExprImpl(
            underlying=expr,
            new_shape=(-1, 1),
        )

    substitutions = tuple(
        (variable, formatted_values(value)) for variable, value in substitutions
    )

    return polymatrix.expression.impl.SubstituteExprImpl(
        underlying=underlying,
        substitutions=substitutions,
    )


def init_subtract_monomials_expr(
    underlying: ExpressionBaseMixin,
    monomials: ExpressionBaseMixin,
):
    return polymatrix.expression.impl.SubtractMonomialsExprImpl(
        underlying=underlying,
        monomials=monomials,
    )


def init_sum_expr(
    underlying: ExpressionBaseMixin,
):
    return polymatrix.expression.impl.SumExprImpl(
        underlying=underlying,
    )


def init_symmetric_expr(
    underlying: ExpressionBaseMixin,
):
    return polymatrix.expression.impl.SymmetricExprImpl(
        underlying=underlying,
    )


def init_to_constant_expr(
    underlying: ExpressionBaseMixin,
):
    return polymatrix.expression.impl.ToConstantExprImpl(
        underlying=underlying,
    )


def init_to_quadratic_expr(
    underlying: ExpressionBaseMixin,
):
    return polymatrix.expression.impl.ToQuadraticExprImpl(
        underlying=underlying,
    )


def init_to_sorted_variables(
    underlying: ExpressionBaseMixin,
):
    return polymatrix.expression.impl.ToSortedVariablesImpl(
        underlying=underlying,
    )


def init_to_symmetric_matrix_expr(
    underlying: ExpressionBaseMixin,
):
    return polymatrix.expression.impl.ToSymmetricMatrixExprImpl(
        underlying=underlying,
    )


def init_transpose_expr(
    underlying: ExpressionBaseMixin,
):
    return polymatrix.expression.impl.TransposeExprImpl(
        underlying=underlying,
    )


def init_truncate_expr(
    underlying: ExpressionBaseMixin,
    degrees: tuple[int],
    variables: ExpressionBaseMixin | None = None,
    inverse: bool | None = None,
):
    if isinstance(degrees, int):
        degrees = (degrees,)

    if inverse is None:
        inverse = False

    return polymatrix.expression.impl.TruncateExprImpl(
        underlying=underlying,
        variables=variables,
        degrees=degrees,
        inverse=inverse,
    )
