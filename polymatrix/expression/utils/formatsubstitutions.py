import typing


def format_substitutions(
    variables: typing.Union[typing.Any, tuple, dict],
    values: float | tuple = None,
):
    """
    (variables = x, values = 1.0)							# ok
    (variables = x, values = np.array(1.0))					# ok
    (variables = (x, y, z), values = 1.0) 					# ok
    (variables = (x, y, z), values = (1.0, 2.0, 3.0))		# ok
    (variables = {x: 1.0, y: 2.0, z: 3.0})					# ok
    (variables = ((x, 1.0), (y, 2.0), (z, 3.0)))			# ok

    (variables = v, values = (1.0, 2.0))					# ok
    (variables = (v1, v2), values = ((1.0, 2.0), (3.0,)))	# ok
    (variables = (v1, v2), values = (1.0, 2.0, 3.0))		# not ok
    """

    if values is not None:
        if isinstance(variables, tuple):
            if isinstance(values, tuple):
                assert len(variables) == len(values), f"{variables=}, {values=}"

            else:
                values = tuple(values for _ in variables)

        else:
            variables = (variables,)
            values = (values,)

        substitutions = zip(variables, values)

    elif isinstance(variables, dict):
        substitutions = variables.items()

    elif isinstance(variables, tuple):
        substitutions = variables

    else:
        raise Exception(f"{variables=}")

    return substitutions
