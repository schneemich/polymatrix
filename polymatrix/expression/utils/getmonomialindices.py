from polymatrix.expressionstate.abc import ExpressionState
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin


def get_monomial_indices(
    state: ExpressionState,
    expression: ExpressionBaseMixin,
) -> tuple[ExpressionState, tuple[int, ...]]:
    state, poly_matrix = expression.apply(state)

    assert poly_matrix.shape[1] == 1, f"{poly_matrix.shape=}"

    def gen_indices():
        for row in range(poly_matrix.shape[0]):
            row_terms = poly_matrix.get_poly(row, 0)

            assert len(row_terms) == 1, f"{row_terms} contains more than one term"

            yield from row_terms.keys()

    return state, tuple(gen_indices())
