from polymatrix.polymatrix.init import init_broadcast_poly_matrix, init_poly_matrix
from polymatrix.utils.getstacklines import FrameSummary
from polymatrix.utils.tooperatorexception import to_operator_exception
from polymatrix.polymatrix.abc import PolyMatrix


def broadcast_poly_matrix(
    left: PolyMatrix,
    right: PolyMatrix,
    stack: tuple[FrameSummary],
) -> PolyMatrix:
    # broadcast left
    if left.shape == (1, 1) and right.shape != (1, 1):
        left = init_broadcast_poly_matrix(
            data=left.get_poly(0, 0),
            shape=right.shape,
        )

    # broadcast right
    elif left.shape != (1, 1) and right.shape == (1, 1):
        right = init_broadcast_poly_matrix(
            data=right.get_poly(0, 0),
            shape=left.shape,
        )

    else:
        if not (left.shape == right.shape):
            raise AssertionError(
                to_operator_exception(
                    message=f"{left.shape} != {right.shape}",
                    stack=stack,
                )
            )

    return left, right
