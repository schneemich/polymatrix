import numpy as np
import sympy

import polymatrix.expression.init

from polymatrix.expression.expression import init_expression, Expression
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.statemonad.abc import StateMonad


FromDataTypes = (
    str
    | np.ndarray
    | sympy.Matrix
    | sympy.Expr
    | tuple
    | ExpressionBaseMixin
    | StateMonad
)


def from_expr_or_none(
    data: FromDataTypes,
) -> Expression | None:
    return init_expression(
        underlying=polymatrix.expression.init.init_from_expr_or_none(
            data=data,
        ),
    )


def from_(
    data: FromDataTypes,
) -> Expression:
    return init_expression(
        underlying=polymatrix.expression.init.init_from_expr(
            data=data,
        ),
    )
