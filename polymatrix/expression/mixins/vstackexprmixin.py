import abc
import itertools
import dataclassabc
from polymatrix.polymatrix.mixins import PolyMatrixMixin

from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.expressionstate.abc import ExpressionState


class VStackExprMixin(ExpressionBaseMixin):
    """
    Vertical stacking of the underlying polynomial matrices

    [[1, 2]], [[3, 4]]  ->  [[1, 2], [3, 4]]
    """

    @property
    @abc.abstractmethod
    def underlying(self) -> tuple[ExpressionBaseMixin, ...]: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionState,
    ) -> tuple[ExpressionState, PolyMatrix]:
        all_underlying = []
        for expr in self.underlying:
            state, polymatrix = expr.apply(state=state)
            all_underlying.append(polymatrix)

        for underlying in all_underlying:
            assert (
                underlying.shape[1] == all_underlying[0].shape[1]
            ), f"{underlying.shape[1]} not equal {all_underlying[0].shape[1]}"

        @dataclassabc.dataclassabc(frozen=True)
        class VStackPolyMatrix(PolyMatrixMixin):
            all_underlying: tuple[PolyMatrixMixin]
            underlying_row_range: tuple[tuple[int, int], ...]
            shape: tuple[int, int]

            def get_poly(self, row: int, col: int) -> dict[tuple[int, ...], float]:
                for polymatrix, (row_start, row_end) in zip(
                    self.all_underlying, self.underlying_row_range
                ):
                    if row_start <= row < row_end:
                        return polymatrix.get_poly(
                            row=row - row_start,
                            col=col,
                        )

                raise Exception(f"row {row} is out of bounds")

        underlying_row_range = tuple(
            itertools.pairwise(
                itertools.accumulate(
                    (expr.shape[0] for expr in all_underlying), initial=0
                )
            )
        )

        n_row = sum(polymatrix.shape[0] for polymatrix in all_underlying)

        polymatrix = VStackPolyMatrix(
            all_underlying=all_underlying,
            shape=(n_row, all_underlying[0].shape[1]),
            underlying_row_range=underlying_row_range,
        )

        return state, polymatrix
