import abc
import collections
import dataclasses
from numpy import var

from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.expressionstate.abc import ExpressionState


class DeterminantExprMixin(ExpressionBaseMixin):
    @property
    @abc.abstractmethod
    def underlying(self) -> ExpressionBaseMixin: ...

    # # overwrites the abstract method of `ExpressionBaseMixin`
    # @property
    # def shape(self) -> tuple[int, int]:
    #     return self.underlying.shape[0], 1

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionState,
    ) -> tuple[ExpressionState, PolyMatrix]:
        # raise Exception('not implemented')

        if self in state.cache:
            return state, state.cache[self]

        state, underlying = self.underlying.apply(state=state)

        assert underlying.shape[0] == underlying.shape[1]

        inequality_data = {}
        auxillary_equations = {}

        index_start = state.n_param
        rel_index = 0

        for row in range(underlying.shape[0]):
            polynomial = collections.defaultdict(float)

            # f in f-v^T@x-r^2
            # terms = underlying.get_poly(row, row)
            try:
                underlying_poly = underlying.get_poly(row, row)
            except KeyError:
                pass
            else:
                for monomial, value in underlying_poly.items():
                    polynomial[monomial] += value

            for inner_row in range(row):
                # -v^T@x in f-v^T@x-r^2
                # terms = underlying.get_poly(row, inner_row)
                try:
                    underlying_poly = underlying.get_poly(row, inner_row)
                except KeyError:
                    pass
                else:
                    for monomial, value in underlying_poly.items():
                        new_monomial = monomial + (index_start + rel_index + inner_row,)
                        polynomial[new_monomial] -= value

                # auxillary terms
                # ---------------

                auxillary_polynomial = collections.defaultdict(float)

                for inner_col in range(row):
                    # P@x in P@x-v
                    key = tuple(reversed(sorted((inner_row, inner_col))))
                    try:
                        underlying_poly = underlying.get_poly(*key)
                    except KeyError:
                        pass
                    else:
                        for monomial, value in underlying_poly.items():
                            new_monomial = monomial + (
                                index_start + rel_index + inner_col,
                            )
                            auxillary_polynomial[new_monomial] += value

                # -v in P@x-v
                try:
                    underlying_poly = underlying.get_poly(row, inner_row)
                except KeyError:
                    pass
                else:
                    for monomial, value in underlying_poly.items():
                        auxillary_polynomial[monomial] -= value

                x_variable = index_start + rel_index + inner_row
                assert x_variable not in state.auxillary_equations
                auxillary_equations[x_variable] = dict(auxillary_polynomial)

            rel_index += row
            inequality_data[row, 0] = dict(polynomial)

        state = state.register(rel_index)

        poly_matrix = init_poly_matrix(
            data=inequality_data,
            shape=(underlying.shape[0], 1),
        )

        state = dataclasses.replace(
            state,
            auxillary_equations=state.auxillary_equations | auxillary_equations,
            cache=state.cache | {self: poly_matrix},
        )

        return state, poly_matrix
