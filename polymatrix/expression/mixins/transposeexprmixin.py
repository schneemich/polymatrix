import abc
import dataclassabc

from polymatrix.polymatrix.mixins import PolyMatrixMixin
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.expressionstate.abc import ExpressionState


class TransposeExprMixin(ExpressionBaseMixin):
    """
    Transpose the polynomial matrix

    [[1, 2, 3]]  ->  [[1], [2], [3]]
    """

    @property
    @abc.abstractmethod
    def underlying(self) -> ExpressionBaseMixin: ...

    # overwrites the abstract method of `PolyMatrixExprBaseMixin`
    def apply(
        self,
        state: ExpressionState,
    ) -> tuple[ExpressionState, PolyMatrix]:
        state, underlying = self.underlying.apply(state=state)

        @dataclassabc.dataclassabc(frozen=True)
        class TransposePolyMatrix(PolyMatrixMixin):
            underlying: PolyMatrixMixin
            shape: tuple[int, int]

            def get_poly(self, row: int, col: int) -> dict[tuple[int, ...], float]:
                return self.underlying.get_poly(col, row)

        return state, TransposePolyMatrix(
            underlying=underlying,
            shape=(underlying.shape[1], underlying.shape[0]),
        )
