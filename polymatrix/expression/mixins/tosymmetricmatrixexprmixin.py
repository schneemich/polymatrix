import abc

from polymatrix.expression.utils.getvariableindices import (
    get_variable_indices_from_variable,
)
from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expressionstate.mixins import ExpressionStateMixin
from polymatrix.polymatrix.mixins import PolyMatrixMixin


class ToSymmetricMatrixExprMixin(ExpressionBaseMixin):
    """
    Convert a vector to a symmetric matrix

    [1, 2, 3, 4, 5, 6].T  ->  [[1, 2, 3], [2, 4, 5], [3, 5, 6]]
    """

    @property
    @abc.abstractclassmethod
    def underlying(self) -> ExpressionBaseMixin: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionStateMixin,
    ) -> tuple[ExpressionStateMixin, PolyMatrixMixin]:
        state, underlying = self.underlying.apply(state)

        assert underlying.shape[1] == 1

        def invert_binomial_coefficient(val):
            idx = 1
            sum_val = 1

            while sum_val < val:
                idx += 1
                sum_val += idx

            assert sum_val == val, f"{sum_val=}, {val=}"

            return idx

        n_row = invert_binomial_coefficient(underlying.shape[0])

        poly_matrix_data = {}
        var_index = 0

        for row in range(n_row):
            for col in range(row, n_row):
                poly_matrix_data[row, col] = underlying.get_poly(var_index, 0)

                if row != col:
                    poly_matrix_data[col, row] = poly_matrix_data[row, col]

                var_index += 1

        poly_matrix = init_poly_matrix(
            data=poly_matrix_data,
            shape=(n_row, n_row),
        )

        return state, poly_matrix
