import abc

from polymatrix.expressionstate.abc import ExpressionState
from polymatrix.polymatrix.abc import PolyMatrix


class ExpressionBaseMixin(abc.ABC):
    @abc.abstractmethod
    def apply(self, state: ExpressionState) -> tuple[ExpressionState, PolyMatrix]: ...
