import abc
import math
import sympy
import numpy as np

from polymatrix.utils.getstacklines import FrameSummary
from polymatrix.utils.tooperatorexception import to_operator_exception
from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.polymatrix.mixins import PolyMatrixMixin
from polymatrix.expressionstate.mixins import ExpressionStateMixin
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin


class FromTupleExprMixin(ExpressionBaseMixin):
    DATA_TYPE = int | float | np.number | sympy.Expr | ExpressionBaseMixin

    @property
    @abc.abstractmethod
    def data(self) -> tuple[tuple["FromTupleExprMixin.DATA_TYPE"]]: ...

    @property
    @abc.abstractmethod
    def stack(self) -> tuple[FrameSummary]: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionStateMixin,
    ) -> tuple[ExpressionStateMixin, PolyMatrixMixin]:
        polynomials = {}

        for poly_row, col_data in enumerate(self.data):
            for poly_col, poly_data in enumerate(col_data):
                if isinstance(poly_data, (bool, np.bool_)):
                    poly_data = int(poly_data)

                if isinstance(poly_data, (int, float, np.number)):
                    if math.isclose(poly_data, 0):
                        polynomial = {}
                    else:
                        polynomial = {tuple(): poly_data}

                elif isinstance(poly_data, sympy.Expr):
                    try:
                        poly = sympy.poly(poly_data)

                    except sympy.polys.polyerrors.GeneratorsNeeded:
                        if not math.isclose(poly_data, 0):
                            polynomials[poly_row, poly_col] = {tuple(): poly_data}
                        continue

                    except ValueError:
                        raise ValueError(f"{poly_data=}")

                    for symbol in poly.gens:
                        state = state.register(key=symbol, n_param=1)
                        # print(f'{symbol}: {state.n_param}')

                    polynomial = {}

                    # a5 x1 x3**2 -> c=a5, m_cnt=(1, 0, 2)
                    for value, monomial_count in zip(poly.coeffs(), poly.monoms()):
                        if math.isclose(value, 0):
                            continue

                        # m_cnt=(1, 0, 2) -> m=(0, 2, 2)
                        def gen_monomial():
                            for rel_idx, p in enumerate(monomial_count):
                                if 0 < p:
                                    idx, _ = state.offset_dict[poly.gens[rel_idx]]
                                    yield idx, p

                        monomial = tuple(gen_monomial())

                        polynomial[monomial] = value

                elif isinstance(poly_data, ExpressionBaseMixin):
                    state, instance = poly_data.apply(state)

                    if not (instance.shape == (1, 1)):
                        raise AssertionError(
                            to_operator_exception(
                                message=f"{instance.shape=} is not (1, 1)",
                                stack=self.stack,
                            )
                        )

                    polynomial = instance.get_poly(0, 0)

                else:
                    raise AssertionError(
                        to_operator_exception(
                            message=f"unknown data type {type(poly_data)=}",
                            stack=self.stack,
                        )
                    )

                polynomials[poly_row, poly_col] = polynomial

        poly_matrix = init_poly_matrix(
            data=polynomials,
            shape=(len(self.data), len(self.data[0])),
        )

        return state, poly_matrix
