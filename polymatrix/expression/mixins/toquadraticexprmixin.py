import abc
import collections
import dataclasses

from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.expressionstate.abc import ExpressionState


class ToQuadraticExprMixin(ExpressionBaseMixin):
    """
    [[1 + x1**2 + x1**3]]  ->  [[1 + x1**2 + x2*x1]]

    with auxilliary equation: x1**2 - x2
    """

    @property
    @abc.abstractmethod
    def underlying(self) -> ExpressionBaseMixin: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionState,
    ) -> tuple[ExpressionState, PolyMatrix]:
        state, underlying = self.underlying.apply(state=state)

        state = [state]

        poly_matrix_data = {}
        auxillary_equations_from_quadratic = {}

        def to_quadratic(monomial_terms):
            polynomial = collections.defaultdict(float)

            for monomial, value in monomial_terms.items():
                if 2 < len(monomial):
                    current_aux = state[0].n_param
                    polynomial[(monomial[0], current_aux)] += value
                    state[0] = state[0].register(n_param=1)

                    for variable in monomial[1:-2]:
                        auxillary_equations_from_quadratic[current_aux] = {
                            (variable, current_aux + 1): 1,
                            (current_aux,): -1,
                        }
                        state[0] = state[0].register(n_param=1)
                        current_aux += 1

                    auxillary_equations_from_quadratic[current_aux] = {
                        (monomial[-2], monomial[-1]): 1,
                        (current_aux,): -1,
                    }

                else:
                    polynomial[monomial] += value

            # return dict(terms_row_col)
            return polynomial

        for row in range(underlying.shape[0]):
            for col in range(underlying.shape[1]):
                polynomial = underlying.get_poly(row, col)
                if polynomial is None:
                    continue

                polynomial = to_quadratic(
                    monomial_terms=polynomial,
                )

                poly_matrix_data[row, col] = polynomial

        def gen_auxillary_equations():
            for key, monomial_terms in state[0].auxillary_equations.items():
                polynomial = to_quadratic(
                    monomial_terms=monomial_terms,
                )
                yield key, polynomial

        state = dataclasses.replace(
            state[0],
            auxillary_equations=dict(gen_auxillary_equations())
            | auxillary_equations_from_quadratic,
        )

        poly_matrix = init_poly_matrix(
            data=poly_matrix_data,
            shape=underlying.shape,
        )

        return state, poly_matrix
