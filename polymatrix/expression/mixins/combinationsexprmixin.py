import abc
import itertools

from polymatrix.polymatrix.utils.multiplypolynomial import multiply_polynomial
from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.expressionstate.abc import ExpressionState
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin


class CombinationsExprMixin(ExpressionBaseMixin):
    """
    combination using degrees=(0, 1, 2, 3):

    [[x]]  ->  [[1], [x], [x**2], [x**3]]
    """

    @property
    @abc.abstractmethod
    def expression(self) -> ExpressionBaseMixin: ...

    @property
    @abc.abstractmethod
    def degrees(self) -> tuple[int, ...]: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionState,
    ) -> tuple[ExpressionState, PolyMatrix]:
        state, poly_matrix = self.expression.apply(state=state)

        assert poly_matrix.shape[1] == 1

        def gen_indices():
            for degree in self.degrees:
                yield from itertools.combinations_with_replacement(
                    range(poly_matrix.shape[0]), degree
                )

        indices = tuple(gen_indices())

        poly_matrix_data = {}

        for row, indexing in enumerate(indices):
            # x.combinations((0, 1, 2)) produces [1, x, x**2]
            if len(indexing) == 0:
                poly_matrix_data[row, 0] = {tuple(): 1.0}
                continue

            def acc_product(left, row):
                right = poly_matrix.get_poly(row, 0)

                if len(left) == 0:
                    return right

                result = {}
                multiply_polynomial(left, right, result)
                return result

            *_, polynomial = itertools.accumulate(
                indexing,
                acc_product,
                initial={},
            )

            poly_matrix_data[row, 0] = polynomial

        poly_matrix = init_poly_matrix(
            data=poly_matrix_data,
            shape=(len(poly_matrix_data), 1),
        )

        return state, poly_matrix
