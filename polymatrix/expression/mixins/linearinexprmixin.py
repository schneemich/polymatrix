import abc
import collections

from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.expressionstate.abc import ExpressionState
from polymatrix.expression.utils.getmonomialindices import get_monomial_indices
from polymatrix.expression.utils.getvariableindices import (
    get_variable_indices_from_variable,
)


class LinearInExprMixin(ExpressionBaseMixin):
    """
    Maps a polynomial column vector

        underlying = [
            [1 + a x],
            [x^2    ],
        ]

    into a polynomial matrix

        output = [
            [1, a, 0],
            [0, 0, 1],
        ],

    where each column corresponds to a monomial defined by

        monomials = [1, x, x^2].
    """

    @property
    @abc.abstractmethod
    def underlying(self) -> ExpressionBaseMixin: ...

    @property
    @abc.abstractmethod
    def monomials(self) -> ExpressionBaseMixin: ...

    @property
    @abc.abstractmethod
    def variables(self) -> ExpressionBaseMixin: ...

    @property
    @abc.abstractmethod
    def ignore_unmatched(self) -> bool: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionState,
    ) -> tuple[ExpressionState, PolyMatrix]:
        state, underlying = self.underlying.apply(state=state)
        state, monomials = get_monomial_indices(state, self.monomials)
        state, variable_indices = get_variable_indices_from_variable(
            state, self.variables
        )

        assert underlying.shape[1] == 1

        poly_matrix_data = collections.defaultdict(dict)

        for row in range(underlying.shape[0]):
            polynomial = underlying.get_poly(row, 0)
            if polynomial is None:
                continue

            for monomial, value in polynomial.items():
                x_monomial = tuple(
                    (var_idx, count)
                    for var_idx, count in monomial
                    if var_idx in variable_indices
                )
                p_monomial = tuple(
                    (var_idx, count)
                    for var_idx, count in monomial
                    if var_idx not in variable_indices
                )

                try:
                    col = monomials.index(x_monomial)
                except ValueError:
                    if self.ignore_unmatched:
                        continue
                    else:
                        raise Exception(f"{x_monomial} not in {monomials}")

                poly_matrix_data[row, col][p_monomial] = value

        poly_matrix = init_poly_matrix(
            data=dict(poly_matrix_data),
            shape=(underlying.shape[0], len(monomials)),
        )

        return state, poly_matrix
