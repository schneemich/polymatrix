import abc
import dataclasses

from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expressionstate.mixins import ExpressionStateMixin
from polymatrix.polymatrix.mixins import PolyMatrixMixin


# remove?
class ParametrizeMatrixExprMixin(ExpressionBaseMixin):
    @property
    @abc.abstractclassmethod
    def underlying(self) -> ExpressionBaseMixin: ...

    @property
    @abc.abstractclassmethod
    def name(self) -> str: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionStateMixin,
    ) -> tuple[ExpressionStateMixin, PolyMatrixMixin]:
        # cache polymatrix to not re-parametrize at every apply call
        if self in state.cache:
            return state, state.cache[self]

        state, underlying = self.underlying.apply(state)

        assert underlying.shape[1] == 1

        poly_matrix_data = {}
        var_index = 0

        for row in range(underlying.shape[0]):
            for _ in range(row, underlying.shape[0]):
                poly_matrix_data[var_index, 0] = {
                    ((state.n_param + var_index, 1),): 1.0
                }

                var_index += 1

        state = state.register(
            key=self,
            n_param=var_index,
        )

        poly_matrix = init_poly_matrix(
            data=poly_matrix_data,
            shape=(var_index, 1),
        )

        state = dataclasses.replace(
            state,
            cache=state.cache | {self: poly_matrix},
        )

        return state, poly_matrix
