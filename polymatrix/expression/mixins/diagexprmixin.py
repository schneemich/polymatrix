import abc
import dataclassabc

from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expressionstate.mixins import ExpressionStateMixin
from polymatrix.polymatrix.mixins import PolyMatrixMixin


class DiagExprMixin(ExpressionBaseMixin):
    """
    [[1],[2]]  ->  [[1,0],[0,2]]

    or

    [[1,0],[0,2]]  ->  [[1],[2]]
    """

    @property
    @abc.abstractclassmethod
    def underlying(self) -> ExpressionBaseMixin: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionStateMixin,
    ) -> tuple[ExpressionStateMixin, PolyMatrixMixin]:
        state, underlying = self.underlying.apply(state)

        if underlying.shape[1] == 1:

            @dataclassabc.dataclassabc(frozen=True)
            class DiagPolyMatrix(PolyMatrixMixin):
                underlying: PolyMatrixMixin
                shape: tuple[int, int]

                def get_poly(self, row: int, col: int) -> dict[tuple[int, ...], float]:
                    if row == col:
                        return self.underlying.get_poly(row, 0)
                    else:
                        return {tuple(): 0.0}

            return state, DiagPolyMatrix(
                underlying=underlying,
                shape=(underlying.shape[0], underlying.shape[0]),
            )

        else:
            assert underlying.shape[0] == underlying.shape[1], f"{underlying.shape=}"

            @dataclassabc.dataclassabc(frozen=True)
            class TracePolyMatrix(PolyMatrixMixin):
                underlying: PolyMatrixMixin
                shape: tuple[int, int]

                def get_poly(self, row: int, _) -> dict[tuple[int, ...], float]:
                    return self.underlying.get_poly(row, row)

            return state, TracePolyMatrix(
                underlying=underlying,
                shape=(underlying.shape[0], 1),
            )
