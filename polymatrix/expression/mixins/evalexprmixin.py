import abc
import itertools
import math

from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.expressionstate.abc import ExpressionState
from polymatrix.expression.utils.getvariableindices import (
    get_variable_indices_from_variable,
)


class EvalExprMixin(ExpressionBaseMixin):
    @property
    @abc.abstractmethod
    def underlying(self) -> ExpressionBaseMixin: ...

    @property
    @abc.abstractmethod
    def substitutions(self) -> tuple: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionState,
    ) -> tuple[ExpressionState, PolyMatrix]:
        state, underlying = self.underlying.apply(state=state)

        def acc_variable_indices_and_values(acc, next):
            state, acc_indices, acc_values = acc
            variable, values = next

            state, indices = get_variable_indices_from_variable(state, variable)

            if indices is None:
                return acc

            if len(values) == 1:
                values = tuple(values[0] for _ in indices)

            else:
                assert (
                    len(indices) == len(values)
                ), f"{variable=}, {indices=} ({len(indices)}), {values=} ({len(values)})"

            return state, acc_indices + indices, acc_values + values

        *_, (state, variable_indices, values) = itertools.accumulate(
            self.substitutions,
            acc_variable_indices_and_values,
            initial=(state, tuple(), tuple()),
        )

        poly_matrix_data = {}

        for row in range(underlying.shape[0]):
            for col in range(underlying.shape[1]):
                underlying_polynomial = underlying.get_poly(row, col)
                if underlying_polynomial is None:
                    continue

                polynomial = {}

                for monomial, value in underlying_polynomial.items():

                    def acc_monomial(acc, next):
                        new_monomial, value = acc
                        variable, count = next

                        if variable in variable_indices:
                            index = variable_indices.index(variable)
                            new_value = value * values[index] ** count
                            return new_monomial, new_value

                        else:
                            return new_monomial + (next,), value

                    *_, (new_monomial, new_value) = tuple(
                        itertools.accumulate(
                            monomial,
                            acc_monomial,
                            initial=(tuple(), value),
                        )
                    )

                    if new_monomial not in polynomial:
                        polynomial[new_monomial] = 0

                    polynomial[new_monomial] += new_value

                    # delete zero entries
                    if math.isclose(polynomial[new_monomial], 0, abs_tol=1e-12):
                        del polynomial[new_monomial]

                if 0 < len(polynomial):
                    poly_matrix_data[row, col] = polynomial

        poly_matrix = init_poly_matrix(
            data=poly_matrix_data,
            shape=underlying.shape,
        )

        return state, poly_matrix

        # if len(self.values) == 1:
        #     values = tuple(self.values[0] for _ in self.variables)

        # else:
        #     values = self.values

        # def filter_valid_variables():
        #     for var, val in zip(self.variables, self.values):
        #         if isinstance(var, ExpressionBaseMixin) or isinstance(var, int) or (var in state.offset_dict):
        #             yield var, val

        # variables, values = zip(*filter_valid_variables())

        # state, variable_indices = get_variable_indices(state, self.variables)

        # if len(self.values) == 1:
        #     values = tuple(self.values[0] for _ in variable_indices)

        # else:
        #     assert len(variable_indices) == len(self.values), f'length of {variable_indices} does not match length of {self.values}'

        #     values = self.values
