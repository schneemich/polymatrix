import abc
import dataclassabc

from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expressionstate.mixins import ExpressionStateMixin
from polymatrix.polymatrix.mixins import PolyMatrixMixin


class RepMatExprMixin(ExpressionBaseMixin):
    @property
    @abc.abstractclassmethod
    def underlying(self) -> ExpressionBaseMixin: ...

    @property
    @abc.abstractclassmethod
    def repetition(self) -> tuple[int, int]: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionStateMixin,
    ) -> tuple[ExpressionStateMixin, PolyMatrixMixin]:
        state, underlying = self.underlying.apply(state)

        @dataclassabc.dataclassabc(frozen=True)
        class RepMatPolyMatrix(PolyMatrixMixin):
            underlying: PolyMatrixMixin
            shape: tuple[int, int]

            def get_poly(self, row: int, col: int) -> dict[tuple[int, ...], float]:
                n_row, n_col = underlying.shape

                rel_row = row % n_row
                rel_col = col % n_col

                return self.underlying.get_poly(rel_row, rel_col)

        return state, RepMatPolyMatrix(
            underlying=underlying,
            shape=tuple(s * r for s, r in zip(underlying.shape, self.repetition)),
        )
