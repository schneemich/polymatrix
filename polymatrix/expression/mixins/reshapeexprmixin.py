import abc
import functools
import itertools
import operator
import dataclassabc
import numpy as np

from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expressionstate.mixins import ExpressionStateMixin
from polymatrix.polymatrix.mixins import PolyMatrixMixin


class ReshapeExprMixin(ExpressionBaseMixin):
    @property
    @abc.abstractclassmethod
    def underlying(self) -> ExpressionBaseMixin: ...

    @property
    @abc.abstractclassmethod
    def new_shape(
        self,
    ) -> tuple[int | ExpressionBaseMixin, int | ExpressionBaseMixin]: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionStateMixin,
    ) -> tuple[ExpressionStateMixin, PolyMatrixMixin]:
        state, underlying = self.underlying.apply(state)

        @dataclassabc.dataclassabc(frozen=True)
        class ReshapePolyMatrix(PolyMatrixMixin):
            underlying: PolyMatrixMixin
            shape: tuple[int, int]
            underlying_shape: tuple[int, int]

            def get_poly(self, row: int, col: int) -> dict[tuple[int, ...], float]:
                index = row + self.shape[0] * col

                underlying_col = int(index / self.underlying_shape[0])
                underlying_row = index - underlying_col * self.underlying_shape[0]

                return self.underlying.get_poly(underlying_row, underlying_col)

        # replace expression by their number of rows
        def acc_new_shape(acc, index):
            state, acc_indices = acc

            # for idx in self.new_shape:
            if isinstance(index, int):
                pass

            elif isinstance(index, ExpressionBaseMixin):
                state, polymatrix = index.apply(state)
                index = polymatrix.shape[0]

            else:
                raise Exception(f"{index=}")

            return state, acc_indices + (index,)

        *_, (state, new_shape) = itertools.accumulate(
            self.new_shape,
            acc_new_shape,
            initial=(state, tuple()),
        )

        # replace '-1' by the remaining number of elements
        if -1 in new_shape:
            n_total = underlying.shape[0] * underlying.shape[1]

            remaining_shape = tuple(e for e in new_shape if e != -1)

            assert len(remaining_shape) + 1 == len(new_shape)

            n_used = functools.reduce(operator.mul, remaining_shape)

            n_remaining = int(n_total / n_used)

            def gen_shape():
                for e in new_shape:
                    if e == -1:
                        yield n_remaining
                    else:
                        yield e

            new_shape = tuple(gen_shape())

        return state, ReshapePolyMatrix(
            underlying=underlying,
            shape=new_shape,
            underlying_shape=underlying.shape,
        )
