import abc
import dataclasses
import dataclassabc
from polymatrix.polymatrix.mixins import PolyMatrixMixin

from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.expressionstate.abc import ExpressionState


class GetItemExprMixin(ExpressionBaseMixin):
    @dataclasses.dataclass(frozen=True)
    class Slice:
        start: int
        step: int
        stop: int

    @property
    @abc.abstractmethod
    def underlying(self) -> ExpressionBaseMixin: ...

    @property
    @abc.abstractmethod
    def index(self) -> tuple[tuple[int, ...], tuple[int, ...]]: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionState,
    ) -> tuple[ExpressionState, PolyMatrix]:
        state, underlying = self.underlying.apply(state=state)

        def get_proper_index(index, shape):
            if isinstance(index, tuple):
                return index

            elif isinstance(index, GetItemExprMixin.Slice):
                if index.start is None:
                    start = 0
                else:
                    start = index.start

                if index.stop is None:
                    stop = shape
                else:
                    stop = index.stop

                if index.step is None:
                    step = 1
                else:
                    step = index.step

                return tuple(range(start, stop, step))

            else:
                return (index,)

        proper_index = (
            get_proper_index(self.index[0], underlying.shape[0]),
            get_proper_index(self.index[1], underlying.shape[1]),
        )

        @dataclassabc.dataclassabc(frozen=True)
        class GetItemPolyMatrix(PolyMatrixMixin):
            underlying: PolyMatrixMixin
            index: tuple[int, int]

            @property
            def shape(self) -> tuple[int, int]:
                return (len(self.index[0]), len(self.index[1]))

            def get_poly(self, row: int, col: int) -> dict[tuple[int, ...], float]:
                try:
                    n_row = self.index[0][row]
                except IndexError:
                    raise IndexError(
                        f"tuple index {row} out of range given {self.index[0]}"
                    )

                try:
                    n_col = self.index[1][col]
                except IndexError:
                    raise IndexError(
                        f"tuple index {col} out of range given {self.index[1]}"
                    )

                return self.underlying.get_poly(n_row, n_col)

        return state, GetItemPolyMatrix(
            underlying=underlying,
            index=proper_index,
        )
