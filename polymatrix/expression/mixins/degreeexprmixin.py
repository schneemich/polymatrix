
import abc

from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.expressionstate.abc import ExpressionState
from polymatrix.utils.getstacklines import FrameSummary


class DegreeExprMixin(ExpressionBaseMixin):
    @property
    @abc.abstractmethod
    def underlying(self) -> ExpressionBaseMixin:
        ...

    @property
    @abc.abstractmethod
    def stack(self) -> tuple[FrameSummary]:
        ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self, 
        state: ExpressionState,
    ) -> tuple[ExpressionState, PolyMatrix]:
        state, underlying = self.underlying.apply(state=state)

        poly_matrix_data = {}

        for row in range(underlying.shape[0]):
            for col in range(underlying.shape[1]):

                polynomial = underlying.get_poly(row, col)

                if polynomial is None or len(polynomial) == 0:
                    poly_matrix_data[row, col] =0

                else:
                    def gen_degrees():
                        for monomial, _ in polynomial.items():
                            yield sum(count for _, count in monomial)
                        
                    poly_matrix_data[row, col] = {tuple(): max(gen_degrees())}

        poly_matrix = init_poly_matrix(
            data=poly_matrix_data,
            shape=underlying.shape,
        )

        return state, poly_matrix   
