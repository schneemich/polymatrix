import abc

from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expressionstate.mixins import ExpressionStateMixin
from polymatrix.polymatrix.mixins import PolyMatrixMixin


# remove?
class MaxExprMixin(ExpressionBaseMixin):
    @property
    @abc.abstractclassmethod
    def underlying(self) -> ExpressionBaseMixin: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionStateMixin,
    ) -> tuple[ExpressionStateMixin, PolyMatrixMixin]:
        state, underlying = self.underlying.apply(state)

        poly_matrix_data = {}

        for row in range(underlying.shape[0]):

            def gen_values():
                for col in range(underlying.shape[1]):
                    polynomial = underlying.get_poly(row, col)
                    if polynomial is None:
                        continue

                    yield polynomial[tuple()]

            values = tuple(gen_values())

            if 0 < len(values):
                poly_matrix_data[row, 0] = {tuple(): max(values)}

        poly_matrix = init_poly_matrix(
            data=poly_matrix_data,
            shape=(underlying.shape[0], 1),
        )

        return state, poly_matrix
