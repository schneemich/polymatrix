import abc
import collections
from numpy import var

from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.expressionstate.abc import ExpressionState
from polymatrix.expression.utils.getvariableindices import (
    get_variable_indices_from_variable,
)


# is this class needed?
class LinearMatrixInExprMixin(ExpressionBaseMixin):
    @property
    @abc.abstractmethod
    def underlying(self) -> ExpressionBaseMixin: ...

    @property
    @abc.abstractmethod
    def variables(self) -> tuple: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionState,
    ) -> tuple[ExpressionState, PolyMatrix]:
        state, underlying = self.underlying.apply(state=state)
        state, variable_index = get_variable_indices_from_variable(
            state, variables=self.variable
        )

        assert len(variable_index) == 1

        poly_matrix_data = collections.defaultdict(dict)

        for row in range(underlying.shape[0]):
            for col in range(underlying.shape[1]):
                underlying_poly = underlying.get_poly(row, col)

                if underlying_poly is None:
                    continue

                for monomial, value in underlying_poly.items():
                    if len(monomial) == 1:
                        variable, _ = monomial[0]

                        if variable == variable_index:
                            poly_matrix_data[row, col][tuple()] = value

                            break

        poly_matrix = init_poly_matrix(
            data=dict(poly_matrix_data),
            shape=underlying.shape,
        )

        return state, poly_matrix
