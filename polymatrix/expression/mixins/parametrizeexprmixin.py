import abc
import dataclasses

from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expressionstate.mixins import ExpressionStateMixin
from polymatrix.polymatrix.mixins import PolyMatrixMixin


class ParametrizeExprMixin(ExpressionBaseMixin):
    @property
    @abc.abstractclassmethod
    def underlying(self) -> ExpressionBaseMixin: ...

    @property
    @abc.abstractclassmethod
    def name(self) -> str: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionStateMixin,
    ) -> tuple[ExpressionStateMixin, PolyMatrixMixin]:
        state, underlying = self.underlying.apply(state)

        assert underlying.shape[1] == 1

        if self.name in state.offset_dict:
            start, end = state.offset_dict[self.name]

            assert underlying.shape[0] == (end - start)

        else:
            start = state.n_param

            state = state.register(
                key=self.name,
                n_param=underlying.shape[0],
            )

        # # cache polymatrix to not re-parametrize at every apply call
        # if self in state.cache:
        #     return state, state.cache[self]

        # state, underlying = self.underlying.apply(state)

        # assert underlying.shape[1] == 1

        poly_matrix_data = {}

        for row in range(underlying.shape[0]):
            var_index = start + row
            poly_matrix_data[row, 0] = {((var_index, 1),): 1}

        poly_matrix = init_poly_matrix(
            data=poly_matrix_data,
            shape=underlying.shape,
        )

        # state = dataclasses.replace(
        #     state,
        #     cache=state.cache | {self: poly_matrix},
        # )

        return state, poly_matrix
