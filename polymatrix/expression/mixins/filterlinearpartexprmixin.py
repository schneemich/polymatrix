import abc
import collections

from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.expressionstate.abc import ExpressionState


# is this class needed?
class FilterLinearPartExprMixin(ExpressionBaseMixin):
    @property
    @abc.abstractmethod
    def underlying(self) -> ExpressionBaseMixin: ...

    @property
    @abc.abstractmethod
    def variable(self) -> int: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionState,
    ) -> tuple[ExpressionState, PolyMatrix]:
        state, underlying = self.underlying.apply(state=state)
        state, variables = self.variables.apply(state=state)

        def gen_variable_monomials():
            for _, term in variables.gen_data():
                assert len(term) == 1, f"{term} should have only a single monomial"

                for monomial in term.keys():
                    yield set(monomial)

        variable_monomials = tuple(gen_variable_monomials())

        poly_matrix_data = {}

        for row in range(underlying.shape[0]):
            for col in range(underlying.shape[1]):
                underlying_polynomial = underlying.get_poly(row, col)
                if underlying_polynomial is None:
                    continue

                polynomial = collections.defaultdict(float)

                for monomial, value in underlying_polynomial.items():
                    for variable_monomial in variable_monomials:
                        remainder = list(monomial)

                        try:
                            for variable in variable_monomial:
                                remainder.remove(variable)

                        except ValueError:
                            continue

                        # take the first that matches
                        if all(
                            variable not in remainder for variable in variable_monomial
                        ):
                            polynomial[remainder] += value

                            break

                poly_matrix_data[row, col] = dict(polynomial)

        poly_matrix = init_poly_matrix(
            data=poly_matrix_data,
            shape=underlying.shape,
        )

        return state, poly_matrix
