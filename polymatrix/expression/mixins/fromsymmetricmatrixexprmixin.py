
import abc
import dataclasses
import math
from polymatrix.expression.utils.getvariableindices import get_variable_indices_from_variable

from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expressionstate.mixins import ExpressionStateMixin
from polymatrix.polymatrix.mixins import PolyMatrixMixin


class FromSymmetricMatrixExprMixin(ExpressionBaseMixin):
    """ inverse of ToSymmetricMatrixExprMixin """

    @property
    @abc.abstractclassmethod
    def underlying(self) -> ExpressionBaseMixin:
        ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self, 
        state: ExpressionStateMixin,
    ) -> tuple[ExpressionStateMixin, PolyMatrixMixin]:

        state, underlying = self.underlying.apply(state)

        assert underlying.shape[0] == underlying.shape[1]

        poly_matrix_data = {}
        var_index = 0

        for row in range(underlying.shape[0]):
            for col in range(row, underlying.shape[1]):
                
                poly_matrix_data[var_index, 0] = underlying.get_poly(row, col)

                var_index += 1

        poly_matrix = init_poly_matrix(
            data=poly_matrix_data,
            shape=(var_index, 1),
        )

        return state, poly_matrix