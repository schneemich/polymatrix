import abc

from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expressionstate.mixins import ExpressionStateMixin
from polymatrix.polymatrix.mixins import PolyMatrixMixin
from polymatrix.polymatrix.typing import MonomialData


PolynomialTupledData = tuple[tuple[MonomialData, float], ...]
PolynomialMatrixTupledData = tuple[tuple[tuple[int, int], PolynomialTupledData], ...]


class FromPolynomialDataExprMixin(ExpressionBaseMixin):
    # an Expression needs to be hashable
    @property
    @abc.abstractmethod
    def data(self) -> PolynomialMatrixTupledData:
        pass

    @property
    @abc.abstractmethod
    def shape(self) -> tuple[int, int]:
        pass

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionStateMixin,
    ) -> tuple[ExpressionStateMixin, PolyMatrixMixin]:
        data = {coord: dict(polynomial) for coord, polynomial in self.data}

        poly_matrix = init_poly_matrix(
            data=data,
            shape=self.shape,
        )

        return state, poly_matrix
