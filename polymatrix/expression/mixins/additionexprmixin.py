import abc
import math

from polymatrix.utils.getstacklines import FrameSummary
from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.expressionstate.abc import ExpressionState
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expression.utils.broadcastpolymatrix import broadcast_poly_matrix


class AdditionExprMixin(ExpressionBaseMixin):
    """
    Adds two polymatrices

        [[2*x1+x2], [x1**2]] + [[3*x2], [x1]]  ->  [[2*x1+4*x2], [x1+x1**2]].

    If one summand is of size (1, 1), then perform broadcast:

        [[2*x1+x2], [x1**2]] + [[x1]]  ->  [[3*x1+x2], [x1+x1**2]].
    """

    @property
    @abc.abstractmethod
    def left(self) -> ExpressionBaseMixin: ...

    @property
    @abc.abstractmethod
    def right(self) -> ExpressionBaseMixin: ...

    @property
    @abc.abstractmethod
    def stack(self) -> tuple[FrameSummary]: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionState,
    ) -> tuple[ExpressionState, PolyMatrix]:
        state, left = self.left.apply(state=state)
        state, right = self.right.apply(state=state)

        left, right = broadcast_poly_matrix(left, right, self.stack)

        poly_matrix_data = {}

        for row in range(left.shape[0]):
            for col in range(left.shape[1]):
                poly_data = {}

                for underlying in (left, right):
                    polynomial = underlying.get_poly(row, col)
                    if polynomial is None:
                        continue

                    if len(poly_data) == 0:
                        poly_data = dict(polynomial)

                    else:
                        for monomial, value in polynomial.items():
                            if monomial not in poly_data:
                                poly_data[monomial] = value

                            else:
                                poly_data[monomial] += value

                                if math.isclose(poly_data[monomial], 0):
                                    del poly_data[monomial]

                if 0 < len(poly_data):
                    poly_matrix_data[row, col] = poly_data

        poly_matrix = init_poly_matrix(
            data=poly_matrix_data,
            shape=left.shape,
        )

        return state, poly_matrix
