import abc
import itertools
from polymatrix.polymatrix.typing import PolynomialData
from polymatrix.polymatrix.utils.multiplypolynomial import multiply_polynomial

from polymatrix.utils.getstacklines import FrameSummary
from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.expressionstate.abc import ExpressionState
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin


class ProductExprMixin(ExpressionBaseMixin):
    @property
    @abc.abstractmethod
    def underlying(self) -> tuple[ExpressionBaseMixin]: ...

    @property
    @abc.abstractmethod
    def degrees(self) -> tuple[int, ...] | None: ...

    @property
    @abc.abstractmethod
    def stack(self) -> tuple[FrameSummary]: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionState,
    ) -> tuple[ExpressionState, PolyMatrix]:
        if len(self.underlying) == 0:
            poly_matrix_data = {(0, 0): {tuple(): 1}}

        else:

            def acc_underlying(acc, v):
                state, acc_polymatrix = acc
                underlying = v

                state, poly_matrix = underlying.apply(state=state)

                return state, acc_polymatrix + (poly_matrix,)

            *_, (state, underlying) = itertools.accumulate(
                self.underlying, acc_underlying, initial=(state, tuple())
            )

            def gen_indices():
                product_indices = itertools.product(
                    *(range(e.shape[0]) for e in underlying)
                )

                if self.degrees is None:
                    yield from product_indices

                else:
                    yield from filter(lambda v: sum(v) in self.degrees, product_indices)

            indices = tuple(gen_indices())

            poly_matrix_data = {}

            for row, indexing in enumerate(indices):

                def acc_product(
                    left: PolynomialData,
                    v: tuple[ExpressionBaseMixin, int],
                ) -> PolynomialData:
                    poly_matrix, row = v

                    right = poly_matrix.get_poly(row, 0)

                    if len(left) == 0:
                        return right

                    result = {}
                    multiply_polynomial(left, right, result)
                    return result

                *_, polynomial = itertools.accumulate(
                    zip(underlying, indexing),
                    acc_product,
                    initial={},
                )

                poly_matrix_data[row, 0] = polynomial

        poly_matrix = init_poly_matrix(
            data=poly_matrix_data,
            shape=(len(poly_matrix_data), 1),
        )

        return state, poly_matrix
