import abc
import collections

from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.expressionstate.abc import ExpressionState
from polymatrix.expression.utils.getmonomialindices import get_monomial_indices
from polymatrix.expression.utils.getvariableindices import (
    get_variable_indices_from_variable,
)
from polymatrix.polymatrix.utils.splitmonomialindices import split_monomial_indices
from polymatrix.utils.getstacklines import FrameSummary
from polymatrix.utils.tooperatorexception import to_operator_exception


class QuadraticInExprMixin(ExpressionBaseMixin):
    """
    [[4 + 2*x1 + 3*x1**2]] -> [[4, 1], [1, 3]]
    """

    @property
    @abc.abstractmethod
    def underlying(self) -> ExpressionBaseMixin: ...

    @property
    @abc.abstractmethod
    def monomials(self) -> ExpressionBaseMixin: ...

    @property
    @abc.abstractmethod
    def variables(self) -> ExpressionBaseMixin: ...

    @property
    @abc.abstractmethod
    def stack(self) -> tuple[FrameSummary]: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionState,
    ) -> tuple[ExpressionState, PolyMatrix]:
        state, underlying = self.underlying.apply(state=state)
        state, sos_monomials = get_monomial_indices(state, self.monomials)
        state, variable_indices = get_variable_indices_from_variable(
            state, self.variables
        )

        if not (underlying.shape == (1, 1)):
            raise AssertionError(
                to_operator_exception(
                    message=f"underlying shape is {underlying.shape}",
                    stack=self.stack,
                )
            )

        poly_matrix_data = collections.defaultdict(
            lambda: collections.defaultdict(float)
        )

        underlying_poly = underlying.get_poly(0, 0)

        if underlying_poly is None:
            raise AssertionError(
                to_operator_exception(
                    message=f"{underlying} is empty",
                    # message=f'{self.underlying} is empty',
                    stack=self.stack,
                )
            )

        for monomial, value in underlying_poly.items():
            x_monomial = tuple(
                (var_idx, count)
                for var_idx, count in monomial
                if var_idx in variable_indices
            )
            p_monomial = tuple(
                (var_idx, count)
                for var_idx, count in monomial
                if var_idx not in variable_indices
            )

            left, right = split_monomial_indices(x_monomial)

            try:
                col = sos_monomials.index(left)
            except ValueError:
                raise AssertionError(
                    to_operator_exception(
                        message=f"{left=} not in {sos_monomials=}",
                        stack=self.stack,
                    )
                )

            try:
                row = sos_monomials.index(right)
            except ValueError:
                raise AssertionError(
                    to_operator_exception(
                        message=f"{right=} not in {sos_monomials=}",
                        stack=self.stack,
                    )
                )

            poly_matrix_data[row, col][p_monomial] += value

        poly_matrix = init_poly_matrix(
            data=dict((k, dict(v)) for k, v in poly_matrix_data.items()),
            shape=2 * (len(sos_monomials),),
        )

        return state, poly_matrix
