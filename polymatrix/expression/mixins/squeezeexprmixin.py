import abc

from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.expressionstate.abc import ExpressionState


# remove?
class SqueezeExprMixin(ExpressionBaseMixin):
    @property
    @abc.abstractmethod
    def underlying(self) -> ExpressionBaseMixin: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionState,
    ) -> tuple[ExpressionState, PolyMatrix]:
        state, underlying = self.underlying.apply(state=state)

        assert underlying.shape[1] == 1

        poly_matrix_data = {}
        row_index = 0

        for row in range(underlying.shape[0]):
            polynomial = underlying.get_poly(row, 0)
            if polynomial is None:
                continue

            polynomial = {}

            for monomial, value in polynomial.items():
                if value != 0.0:
                    polynomial[monomial] = value

            if len(polynomial):
                poly_matrix_data[row_index, 0] = polynomial
                row_index += 1

        poly_matrix = init_poly_matrix(
            data=poly_matrix_data,
            shape=(row_index, 1),
        )

        return state, poly_matrix
