import abc

from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.expressionstate.abc import ExpressionState


class FilterExprMixin(ExpressionBaseMixin):
    """
    [[x1, x2, x3]], [[1, 0, 1]]  ->  [[x1, x3]].
    """

    @property
    @abc.abstractmethod
    def underlying(self) -> ExpressionBaseMixin: ...

    @property
    @abc.abstractmethod
    def predicator(self) -> ExpressionBaseMixin: ...

    @property
    @abc.abstractmethod
    def inverse(self) -> bool: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionState,
    ) -> tuple[ExpressionState, PolyMatrix]:
        state, underlying = self.underlying.apply(state=state)
        state, predicator = self.predicator.apply(state=state)

        # todo: generalize to nxn matrix
        assert underlying.shape[1] == 1
        assert predicator.shape[1] == 1
        assert underlying.shape[0] == predicator.shape[0]

        poly_matrix_data = {}
        row_index = 0

        for row in range(underlying.shape[0]):
            underlying_polynomial = underlying.get_poly(row, 0)

            if underlying_polynomial is None:
                continue

            predicator_poly = predicator.get_poly(row, 0)

            if predicator_poly is None:
                predicator_value = 0

            else:
                key = tuple()

                if key in predicator_poly:
                    predicator_value = round(predicator_poly[key])

                    if isinstance(predicator_value, (float, bool)):
                        predicator_value = int(predicator_value)

                else:
                    predicator_value = 0

            if (predicator_value != 0) is not self.inverse:
                poly_matrix_data[row_index, 0] = underlying_polynomial
                row_index += 1

        poly_matrix = init_poly_matrix(
            data=poly_matrix_data,
            shape=(row_index, 1),
        )

        return state, poly_matrix
