import abc
import dataclasses

from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expressionstate.mixins import ExpressionStateMixin
from polymatrix.polymatrix.mixins import PolyMatrixMixin
from polymatrix.expression.utils.getmonomialindices import get_monomial_indices
from polymatrix.polymatrix.utils.sortmonomials import sort_monomials
from polymatrix.polymatrix.utils.subtractmonomialindices import (
    SubtractError,
    subtract_monomial_indices,
)


class SubtractMonomialsExprMixin(ExpressionBaseMixin):
    """
    [[1], [x], [x**3]], [[x]]  ->  [[1], [x**2]]
    """

    @property
    @abc.abstractclassmethod
    def underlying(self) -> ExpressionBaseMixin: ...

    @property
    @abc.abstractmethod
    def monomials(self) -> ExpressionBaseMixin: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionStateMixin,
    ) -> tuple[ExpressionStateMixin, PolyMatrixMixin]:
        state, underlying = get_monomial_indices(state, self.underlying)
        state, sub_monomials = get_monomial_indices(state, self.monomials)

        def gen_remainders():
            for m1 in underlying:
                for m2 in sub_monomials:
                    try:
                        remainder = subtract_monomial_indices(m1, m2)
                    except SubtractError:
                        # continue
                        remainder = tuple()

                    yield remainder

        remainders = sort_monomials(set(gen_remainders()))

        data = {(row, 0): {remainder: 1.0} for row, remainder in enumerate(remainders)}

        poly_matrix = init_poly_matrix(
            data=data,
            shape=(len(remainders), 1),
        )

        state = dataclasses.replace(
            state,
            cache=state.cache | {self: poly_matrix},
        )

        return state, poly_matrix
