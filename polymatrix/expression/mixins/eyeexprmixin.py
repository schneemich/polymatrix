import abc
import itertools
import dataclassabc
from polymatrix.polymatrix.mixins import PolyMatrixMixin

from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.expressionstate.abc import ExpressionState


class EyeExprMixin(ExpressionBaseMixin):
    @property
    @abc.abstractmethod
    def variable(self) -> ExpressionBaseMixin: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionState,
    ) -> tuple[ExpressionState, PolyMatrix]:
        state, variable = self.variable.apply(state)

        @dataclassabc.dataclassabc(frozen=True)
        class EyePolyMatrix(PolyMatrixMixin):
            shape: tuple[int, int]

            def get_poly(self, row: int, col: int) -> dict[tuple[int, ...], float]:
                if max(row, col) <= self.shape[0]:
                    if row == col:
                        return {tuple(): 1.0}

                    else:
                        return None

                else:
                    raise Exception(f"{(row, col)=} is out of bounds")

        n_row = variable.shape[0]

        polymatrix = EyePolyMatrix(
            shape=(n_row, n_row),
        )

        return state, polymatrix
