from typing import Callable

from polymatrix.statemonad.impl import StateMonadImpl


def init_state_monad(
    apply_func: Callable,
):
    return StateMonadImpl(
        apply_func=apply_func,
    )
