from typing import Callable
import dataclassabc

from polymatrix.statemonad.abc import StateMonad


@dataclassabc.dataclassabc(frozen=True)
class StateMonadImpl(StateMonad):
    apply_func: Callable
