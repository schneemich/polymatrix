import abc

from polymatrix.statemonad.mixins import StateMonadMixin


class StateMonad(StateMonadMixin, abc.ABC):
    pass
