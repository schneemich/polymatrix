import abc
import dataclasses
from typing import Callable, Tuple, TypeVar, Generic


class StateCacheMixin(abc.ABC):
    @property
    @abc.abstractmethod
    def cache(self) -> dict: ...


State = TypeVar("State", bound=StateCacheMixin)
U = TypeVar("U")
V = TypeVar("V")


class StateMonadMixin(
    Generic[State, U],
    abc.ABC,
):
    @property
    @abc.abstractmethod
    def apply_func(self) -> Callable[[State], tuple[State, U]]: ...

    def map(self, fn: Callable[[U], V]) -> "StateMonadMixin[State, V]":
        def internal_map(state: State) -> Tuple[State, U]:
            n_state, val = self.apply(state)
            return n_state, fn(val)

        return dataclasses.replace(self, apply_func=internal_map)

    def flat_map(
        self, fn: Callable[[U], "StateMonadMixin"]
    ) -> "StateMonadMixin[State, V]":
        def internal_map(state: State) -> Tuple[State, V]:
            n_state, val = self.apply(state)
            return fn(val).apply(n_state)

        return dataclasses.replace(self, apply_func=internal_map)

    def zip(self, other: "StateMonadMixin") -> "StateMonadMixin":
        def internal_map(state: State) -> Tuple[State, V]:
            state, val1 = self.apply(state)
            state, val2 = other.apply(state)
            return state, (val1, val2)

        return dataclasses.replace(self, apply_func=internal_map)

    def cache(self) -> "StateMonadMixin":
        def internal_map(state: State) -> Tuple[State, V]:
            if self in state.cache:
                return state, state.cache[self]

            state, val = self.apply(state)

            state = dataclasses.replace(
                state,
                cache=state.cache | {self: val},
            )

            return state, val

        return dataclasses.replace(self, apply_func=internal_map)

    def apply(self, state: State) -> Tuple[State, U]:
        return self.apply_func(state)

    def read(self, state: State) -> U:
        return self.apply_func(state)[1]
