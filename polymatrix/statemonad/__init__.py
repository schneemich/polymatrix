from polymatrix.statemonad.init import init_state_monad
from polymatrix.statemonad.abc import StateMonad


def from_(val):
    def func(state):
        return state, val

    return init_state_monad(func)


def zip(monads: tuple[StateMonad]):
    def zip_func(state):
        values = tuple()

        for monad in monads:
            state, val = monad.apply(state)
            values += (val,)

        return state, values

    return init_state_monad(zip_func)
