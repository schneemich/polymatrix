import dataclassabc

from polymatrix.expressionstate.abc import ExpressionState


@dataclassabc.dataclassabc(frozen=True)
class ExpressionStateImpl(ExpressionState):
    n_param: int
    offset_dict: dict
    auxillary_equations: dict[int, dict[tuple[int], float]]
    cache: dict
