from polymatrix.expressionstate.impl import ExpressionStateImpl


def init_expression_state(
    n_param: int = None,
    offset_dict: dict = None,
):
    if n_param is None:
        n_param = 0

    if offset_dict is None:
        offset_dict = {}

    return ExpressionStateImpl(
        n_param=n_param,
        offset_dict=offset_dict,
        auxillary_equations={},
        cache={},
    )
