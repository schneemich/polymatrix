import itertools


def variable_indices_to_column_index(
    n_var: int,
    variable_indices: tuple[int, ...],
) -> int:
    variable_indices_perm = itertools.permutations(variable_indices)

    return set(
        sum(idx * (n_var**level) for level, idx in enumerate(monomial))
        for monomial in variable_indices_perm
    )
