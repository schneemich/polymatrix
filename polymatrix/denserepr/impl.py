import dataclasses
import itertools
import typing
import numpy as np
import scipy.sparse

from polymatrix.expressionstate.abc import ExpressionState
from polymatrix.expression.utils.getvariableindices import (
    get_variable_indices_from_variable,
)


@dataclasses.dataclass
class DenseReprBufferImpl:
    data: dict[int, np.ndarray]
    n_row: int
    n_param: int

    def get_max_degree(self):
        return max(degree for degree in self.data.keys())

    def add_buffer(self, index: int):
        if index <= 1:
            buffer = np.zeros((self.n_row, self.n_param**index), dtype=np.double)

        else:
            buffer = scipy.sparse.dok_array(
                (self.n_row, self.n_param**index), dtype=np.double
            )

        self.data[index] = buffer

    def add(self, row: int, col: int, index: int, value: float):
        if index not in self.data:
            self.add_buffer(index)

        self.data[index][row, col] = value

    def __getitem__(self, key):
        if key not in self.data:
            self.add_buffer(key)

        return self.data[key]


@dataclasses.dataclass
class DenseReprImpl:
    data: tuple[DenseReprBufferImpl, ...]
    aux_data: typing.Optional[DenseReprBufferImpl]
    variable_mapping: tuple[int, ...]
    state: ExpressionState

    def merge_matrix_equations(self):
        def gen_matrices(index: int):
            for equations in self.data:
                if index < len(equations):
                    yield equations[index]

            if index < len(self.aux_data):
                yield self.aux_data[index]

        indices = set(
            key
            for equations in self.data + (self.aux_data,)
            for key in equations.keys()
        )

        def gen_matrices():
            for index in indices:
                if index <= 1:
                    yield index, np.vstack(tuple(gen_matrices(index)))
                else:
                    yield index, scipy.sparse.vstack(tuple(gen_matrices(index)))

        return dict(gen_matrices())

    def get_value(self, variable, value):
        variable_indices = get_variable_indices_from_variable(self.state, variable)[1]

        def gen_value_index():
            for variable_index in variable_indices:
                try:
                    yield self.variable_mapping.index(variable_index)
                except ValueError:
                    raise ValueError(
                        f"{variable_index} not found in {self.variable_mapping}"
                    )

        value_index = list(gen_value_index())

        return value[value_index]

    def set_value(self, variable, value):
        variable_indices = get_variable_indices_from_variable(self.state, variable)[1]
        value_index = list(
            self.variable_mapping.index(variable_index)
            for variable_index in variable_indices
        )
        vec = np.zeros(len(self.variable_mapping))
        vec[value_index] = value
        return vec

    # def get_matrix(self, eq_idx: int):
    #     equations = self.data[eq_idx].data

    def get_func(self, eq_idx: int):
        equations = self.data[eq_idx].data
        max_idx = max(equations.keys())

        if 2 <= max_idx:

            def func(x: np.ndarray) -> np.ndarray:
                if isinstance(x, tuple) or isinstance(x, list):
                    x = np.array(x).reshape(-1, 1)

                elif x.shape[0] == 1:
                    x = x.reshape(-1, 1)

                def acc_x_powers(acc, _):
                    next = (acc @ x.T).reshape(-1, 1)
                    return next

                x_powers = tuple(
                    itertools.accumulate(
                        range(max_idx - 1),
                        acc_x_powers,
                        initial=x,
                    )
                )[1:]

                def gen_value():
                    for idx, equation in equations.items():
                        if idx == 0:
                            yield equation

                        elif idx == 1:
                            yield equation @ x

                        else:
                            yield equation @ x_powers[idx - 2]

                return sum(gen_value())

        else:

            def func(x: np.ndarray) -> np.ndarray:
                if isinstance(x, tuple) or isinstance(x, list):
                    x = np.array(x).reshape(-1, 1)

                def gen_value():
                    for idx, equation in equations.items():
                        if idx == 0:
                            yield equation

                        else:
                            yield equation @ x

                return sum(gen_value())

        return func
