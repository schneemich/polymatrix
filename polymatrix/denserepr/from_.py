import itertools
import numpy as np

from polymatrix.statemonad.init import init_state_monad
from polymatrix.statemonad.mixins import StateMonadMixin
from polymatrix.expressionstate.abc import ExpressionState
from polymatrix.expression.expression import Expression
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expression.utils.getvariableindices import (
    get_variable_indices_from_variable,
)
from polymatrix.denserepr.utils.monomialtoindex import variable_indices_to_column_index
from polymatrix.denserepr.impl import DenseReprBufferImpl, DenseReprImpl


def from_polymatrix(
    expressions: Expression | tuple[Expression],
    variables: Expression = None,
    sorted: bool = None,
) -> StateMonadMixin[
    ExpressionState, tuple[tuple[tuple[np.ndarray, ...], ...], tuple[int, ...]]
]:
    if isinstance(expressions, Expression):
        expressions = (expressions,)

    assert (
        isinstance(variables, ExpressionBaseMixin) or variables is None
    ), f"{variables=}"

    def func(state: ExpressionState):
        def acc_underlying_application(acc, v):
            state, underlying_list = acc

            state, underlying = v.apply(state)

            assert underlying.shape[1] == 1, f"{underlying.shape[1]=} is not 1"

            return state, underlying_list + (underlying,)

        *_, (state, polymatrix_list) = tuple(
            itertools.accumulate(
                expressions,
                acc_underlying_application,
                initial=(state, tuple()),
            )
        )

        if variables is None:
            sorted_variable_index = tuple()

        else:
            state, variable_index = get_variable_indices_from_variable(state, variables)

            if sorted:
                tagged_variable_index = tuple(
                    (offset, state.get_name_from_offset(offset))
                    for offset in variable_index
                )

                sorted_variable_index = tuple(
                    v[0] for v in sorted(tagged_variable_index, key=lambda v: v[1])
                )

            else:
                sorted_variable_index = variable_index

            sorted_variable_index_set = set(sorted_variable_index)
            if len(sorted_variable_index) != len(sorted_variable_index_set):
                duplicates = tuple(
                    state.get_name_from_offset(var)
                    for var in sorted_variable_index_set
                    if 1 < sorted_variable_index.count(var)
                )
                raise Exception(
                    f"{duplicates=}. Make sure you give a unique name for each variables."
                )

        variable_index_map = {old: new for new, old in enumerate(sorted_variable_index)}

        n_param = len(sorted_variable_index)

        def gen_numpy_matrices():
            for polymatrix in polymatrix_list:
                n_row = polymatrix.shape[0]

                buffer = DenseReprBufferImpl(
                    data={},
                    n_row=n_row,
                    n_param=n_param,
                )

                for row in range(n_row):
                    polymatrix_terms = polymatrix.get_poly(row, 0)

                    if polymatrix_terms is None:
                        continue

                    if len(polymatrix_terms) == 0:
                        buffer.add(row, 0, 0, 0)

                    else:
                        for monomial, value in polymatrix_terms.items():

                            def gen_new_monomial():
                                for var, count in monomial:
                                    try:
                                        index = variable_index_map[var]
                                    except KeyError:
                                        # todo: improve this error message!
                                        raise KeyError(
                                            f"{var=} ({state.get_key_from_offset(var)}) is incompatible with {variable_index_map=}"
                                        )

                                    for _ in range(count):
                                        yield index

                            new_variable_indices = tuple(gen_new_monomial())

                            cols = variable_indices_to_column_index(
                                n_param, new_variable_indices
                            )

                            col_value = value / len(cols)

                            for col in cols:
                                degree = sum(count for _, count in monomial)
                                buffer.add(row, col, degree, col_value)

                yield buffer

        underlying_matrices = tuple(gen_numpy_matrices())

        def gen_auxillary_equations():
            for key, monomial_terms in state.auxillary_equations.items():
                if key in sorted_variable_index:
                    yield key, monomial_terms

        auxillary_equations = tuple(gen_auxillary_equations())

        n_row = len(auxillary_equations)

        if n_row == 0:
            auxillary_matrix_equations = None

        else:
            buffer = DenseReprBufferImpl(
                data={},
                n_row=n_row,
                n_param=n_param,
            )

            for row, (key, monomial_terms) in enumerate(auxillary_equations):
                for monomial, value in monomial_terms.items():
                    new_monomial = tuple(
                        variable_index_map[var]
                        for var, count in monomial
                        for _ in range(count)
                    )

                    cols = variable_indices_to_column_index(n_param, new_monomial)

                    col_value = value / len(cols)

                    for col in cols:
                        buffer.add(
                            row, col, sum(count for _, count in monomial), col_value
                        )

            auxillary_matrix_equations = buffer

        result = DenseReprImpl(
            data=underlying_matrices,
            aux_data=auxillary_matrix_equations,
            variable_mapping=sorted_variable_index,
            state=state,
        )

        return state, result

    return init_state_monad(func)
