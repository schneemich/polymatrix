from polymatrix.expressionstate.abc import ExpressionState as internal_ExpressionState
from polymatrix.expressionstate.init import (
    init_expression_state as internal_init_expression_state,
)
from polymatrix.expression.expression import Expression as internal_Expression
from polymatrix.expression.from_ import from_ as internal_from
from polymatrix.expression import v_stack as internal_v_stack
from polymatrix.expression import h_stack as internal_h_stack
from polymatrix.expression import product as internal_product
from polymatrix.expression.to import to_constant as internal_to_constant
from polymatrix.expression.to import to_sympy as internal_to_sympy
from polymatrix.denserepr.from_ import from_polymatrix

Expression = internal_Expression
ExpressionState = internal_ExpressionState

init_expression_state = internal_init_expression_state
from_ = internal_from
v_stack = internal_v_stack
h_stack = internal_h_stack
product = internal_product
to_constant_repr = internal_to_constant
to_constant = internal_to_constant
to_sympy_repr = internal_to_sympy
to_sympy = internal_to_sympy
to_matrix_repr = from_polymatrix
to_dense = from_polymatrix
