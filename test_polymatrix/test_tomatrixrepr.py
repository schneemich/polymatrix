import unittest
import polymatrix.expression.initexpressionbase

from polymatrix.expressionstate.init.initexpressionstate import init_expression_state


class TestToMatrixRepr(unittest.TestCase):

    def test_1(self):
        underlying_terms = {
            (0, 0): {
                tuple(): 1.0,
                ((1, 1),): 2.0,
            },
            (1, 0): {
                ((0, 1),): 4.0,
                ((0, 1), (1, 1)): 3.0,
                ((1, 2),): 5.0,
            },
            (2, 0): {
                ((0, 1), (1, 2)): 3.0,
            },
        }

        variable_terms = {
            (0, 0): {((0, 1),): 1},
            (1, 0): {((1, 1),): 1},
        }

        expr = polymatrix.expression.initexpressionbase.init_from_terms_expr(terms=underlying_terms, shape=(3, 1))

        state = init_expression_state(n_param=2)
        state, result = polymatrix.to_matrix_repr(
            (expr,), 
            polymatrix.expression.initexpressionbase.init_from_terms_expr(terms=variable_terms, shape=(2, 1),),
        ).apply(state)

        A0 = result.data[0][0]
        A1 = result.data[0][1]
        A2 = result.data[0][2]
        A3 = result.data[0][3]

        self.assertEquals(A0[0, 0], 1.0)

        self.assertEquals(A1[0, 1], 2.0)
        self.assertEquals(A1[1, 0], 4.0)

        self.assertEquals(A2[1, 1], 1.5)
        self.assertEquals(A2[1, 2], 1.5)
        self.assertEquals(A2[1, 3], 5.0)

        self.assertEquals(A3[2, 3], 1.0)
        self.assertEquals(A3[2, 5], 1.0)
        self.assertEquals(A3[2, 6], 1.0)
