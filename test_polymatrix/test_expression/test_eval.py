import unittest

from polymatrix.expressionstate.init.initexpressionstate import init_expression_state
import polymatrix.expression.initexpressionbase


class TestEval(unittest.TestCase):

    def test_1(self):
        terms = {
            (0, 0): {
                ((0, 1), (2, 1)): 2.0,
                ((0, 1), (1, 1), (3, 1)): 3.0,
            }, (1, 0):{
                tuple(): 1.0,
                ((1, 2),): 1.0,
                ((2, 1),): 1.0,
            },
        }

        expr = polymatrix.expression.initexpressionbase.init_eval_expr(
            underlying=polymatrix.expression.initexpressionbase.init_from_terms_expr(terms=terms, shape=(2, 1)),
            variables=(0, 1),
            values=(2.0, 3.0),
        )

        state = init_expression_state(n_param=2)
        state, val = expr.apply(state)

        data = val.get_poly(0, 0)
        self.assertDictEqual({
            ((2, 1),): 4.0,
            ((3, 1),): 18.0,
        }, data)

        data = val.get_poly(1, 0)
        self.assertDictEqual({
            tuple(): 10.0,
            ((2, 1),): 1,
        }, data)
