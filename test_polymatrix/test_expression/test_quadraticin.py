import unittest

from polymatrix.expressionstate.init.initexpressionstate import init_expression_state
import polymatrix.expression.initexpressionbase


class TestQuadraticIn(unittest.TestCase):

    def test_1(self):
        underlying_terms = {
            (0, 0): {
                ((0, 1),): 1.0,  # x1
                ((0, 1), (2, 1)): 2.0,  # x1 
                ((0, 2), (3, 1)): 3.0,  # x1 x1
                ((0, 2), (1, 2), (4, 1)): 4.0,  # x1 x1 x2 x2
                ((0, 2), (1, 1), (5, 1)): 5.0,  # x1 x1 x2
            }
        }

        monomial_terms = {
            (0, 0): {
                tuple(): 1.0,
            },
            (1, 0): {
                ((0, 1),): 1.0,
            },
            (2, 0): {
                ((0, 1), (1, 1)): 1.0,
            },
        }

        variable_terms = {
            (0, 0): {((0, 1),): 1},
            (1, 0): {((1, 1),): 1},
        }

        expr = polymatrix.expression.initexpressionbase.init_quadratic_in_expr(
            underlying=polymatrix.expression.initexpressionbase.init_from_terms_expr(terms=underlying_terms, shape=(1, 1)),
            monomials=polymatrix.expression.initexpressionbase.init_from_terms_expr(terms=monomial_terms, shape=(3, 1)),
            variables=polymatrix.expression.initexpressionbase.init_from_terms_expr(terms=variable_terms, shape=(2, 1),),
        )

        state = init_expression_state(n_param=2)
        state, val = expr.apply(state)

        data = val.get_poly(0, 1)
        self.assertDictContainsSubset({
            tuple(): 1.0,
            ((2, 1),): 2.0,
        }, data)

        data = val.get_poly(1, 1)
        self.assertDictContainsSubset({
            ((3, 1),): 3.0,
        }, data)

        data = val.get_poly(2, 2)
        self.assertDictContainsSubset({
            ((4, 1),): 4.0,
        }, data)

        data = val.get_poly(1, 2)
        self.assertDictContainsSubset({
            ((5, 1),): 5.0,
        }, data)
    