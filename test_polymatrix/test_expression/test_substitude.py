import unittest

from polymatrix.expressionstate.init.initexpressionstate import init_expression_state
import polymatrix.expression.initexpressionbase


class TestEval(unittest.TestCase):

    def test_1(self):
        terms = {
            (0, 0): {
                tuple(): 2.0,
                ((0, 2),): 3.0,
                ((1, 1),): 1.0,
                ((2, 2),): 1.0,
            },
        }

        substitution = {
            (0, 0): {
                ((1, 1),): 1.0,
                ((2, 1),): 1.0,
            },
        }

        variable_terms = {
            (0, 0): {((0, 1),): 1},
        }

        expr = polymatrix.expression.initexpressionbase.init_substitute_expr(
            underlying=polymatrix.expression.initexpressionbase.init_from_terms_expr(terms=terms, shape=(1, 1)),
            variables=polymatrix.expression.initexpressionbase.init_from_terms_expr(terms=variable_terms, shape=(1, 1),),
            values=(polymatrix.expression.initexpressionbase.init_from_terms_expr(terms=substitution, shape=(1, 1)),),
        )

        state = init_expression_state(n_param=2)
        state, val = expr.apply(state)

        data = val.get_poly(0, 0)
        self.assertDictEqual({
            tuple(): 2.0,
            ((1, 1),): 1.0,
            ((1, 2),): 3.0,
            ((1, 1), (2, 1)): 6.0,
            ((2, 2),): 4.0
        }, data)
