import unittest

from polymatrix.expressionstate.init.initexpressionstate import init_expression_state
import polymatrix.expression.initexpressionbase


class TestAddition(unittest.TestCase):

    def test_1(self):
        left_terms = {
            (0, 0): {
                tuple(): 1.0,
                ((0, 1),): 1.0,
            }, 
            (1, 0): {
                ((0, 2),): 1.0,
            },
        }

        right_terms = {
            (0, 0): {
                tuple(): 3.0,
                ((1, 1),): 2.0,
            },
            (1, 1): {
                tuple(): 1.0,
            },
        }

        left = polymatrix.expression.initexpressionbase.init_from_terms_expr(
            terms=left_terms,
            shape=(2, 2),
        )

        right = polymatrix.expression.initexpressionbase.init_from_terms_expr(
            terms=right_terms,
            shape=(2, 2),
        )

        expr = polymatrix.expression.initexpressionbase.init_addition_expr(
            left=left,
            right=right,
        )

        state = init_expression_state(n_param=2)
        state, val = expr.apply(state)

        data = val.get_poly(0, 0)
        self.assertDictContainsSubset({
            tuple(): 4.0,
            ((0, 1),): 1.0,
            ((1, 1),): 2.0,
        }, data)

        data = val.get_poly(1, 0)
        self.assertDictContainsSubset({
            ((0, 2),): 1.0,
        }, data)

        data = val.get_poly(1, 1)
        self.assertDictContainsSubset({
            tuple(): 1.0,
        }, data)
