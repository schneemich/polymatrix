import unittest

from polymatrix.expressionstate.init.initexpressionstate import init_expression_state
import polymatrix.expression.initexpressionbase


class TestToConstant(unittest.TestCase):

    def test_1(self):
        terms = {
            (0, 0): {
                tuple(): 2.0,
                ((0, 1),): 1.0,
            },
            (1, 0): {
                ((0, 2), (1, 1)): 1.0,
                ((0, 3), (1, 1)): 1.0,
            },
            (0, 1): {
                tuple(): 5.0,
                ((0, 2), (2, 1),): 1.0,
                ((3, 1),): 1.0,
            },
        }

        expr = polymatrix.expression.initexpressionbase.init_to_constant_expr(
            underlying=polymatrix.expression.initexpressionbase.init_from_terms_expr(terms=terms, shape=(2, 2)),
        )

        state = init_expression_state(n_param=2)
        state, val = expr.apply(state)

        data = val.get_poly(0, 0)
        self.assertDictEqual({
            tuple(): 2.0,
        }, data)

        data = val.get_poly(0, 1)
        self.assertDictEqual({
            tuple(): 5.0,
        }, data)
    