import unittest
from polymatrix.expression.initexpressionbase import init_from_

from polymatrix.expressionstate.init.initexpressionstate import init_expression_state
import polymatrix.expression.initexpressionbase


class TestLinearIn(unittest.TestCase):

    def test_1(self):
        underlying_terms = {
            (0, 0): {
                ((0, 1),): 2.0,
                ((1, 1),): 3.0,
            },
        }

        monomial_terms = {
            (0, 0): {
                ((0, 1),): 1.0,
            },
            (1, 0): {
                ((2, 1),): 1.0,
            },
            (2, 0): {
                ((1, 1),): 1.0,
            },
            (3, 0): {
                ((3, 1),): 1.0,
            },
        }

        variable_terms = {
            (0, 0): {((0, 1),): 1},
            (1, 0): {((1, 1),): 1},
        }

        expr = polymatrix.expression.initexpressionbase.init_linear_in_expr(
            underlying=polymatrix.expression.initexpressionbase.init_from_terms_expr(terms=underlying_terms, shape=(2, 1)),
            monomials=polymatrix.expression.initexpressionbase.init_from_terms_expr(terms=monomial_terms, shape=(4, 1),),
            variables=polymatrix.expression.initexpressionbase.init_from_terms_expr(terms=variable_terms, shape=(2, 1),),
        )

        state = init_expression_state(n_param=2)
        state, val = expr.apply(state)

        data = val.get_poly(0, 0)
        self.assertDictEqual({
            tuple(): 2.0,
        }, data)

        data = val.get_poly(0, 2)
        self.assertDictEqual({
            tuple(): 3.0,
        }, data)
