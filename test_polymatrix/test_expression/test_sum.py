import unittest

from polymatrix.expressionstate.init.initexpressionstate import init_expression_state
import polymatrix.expression.initexpressionbase


class TestSum(unittest.TestCase):

    def test_1(self):
        terms = {
            (0, 0): {
                tuple(): 2.0,
                ((0, 1),): 3.0,
            }, 
            (0, 1):{
                tuple(): 1.0,
                ((0, 2),): 1.0,
            },
        }

        expr = polymatrix.expression.initexpressionbase.init_sum_expr(
            underlying=polymatrix.expression.initexpressionbase.init_from_terms_expr(terms=terms, shape=(1, 2)),
        )

        state = init_expression_state(n_param=2)
        state, val = expr.apply(state)

        data = val.get_poly(0, 0)
        self.assertDictEqual({
            tuple(): 3.0,
            ((0, 1),): 3.0,
            ((0, 2),): 1.0,
        }, data)
