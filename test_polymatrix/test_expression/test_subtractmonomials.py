import unittest

from polymatrix.expressionstate.init.initexpressionstate import init_expression_state
import polymatrix.expression.initexpressionbase


class TestDerivative(unittest.TestCase):

    def test_1(self):
        monomials1 = {
            (0, 0): {
                ((0, 1),): 1.0,
            },
            (1, 0): {
                ((0, 1), (1, 2)): 1.0,
            },
        }

        monomials2 = {
            (0, 0): {
                ((0, 1),): 1.0,
            },
            (1, 0): {
                ((1, 1),): 1.0,
            },
        }

        expr = polymatrix.expression.initexpressionbase.init_subtract_monomials_expr(
            underlying=polymatrix.expression.initexpressionbase.init_from_terms_expr(terms=monomials1, shape=(2, 1)),
            monomials=polymatrix.expression.initexpressionbase.init_from_terms_expr(terms=monomials2, shape=(2, 1)),
        )

        state = init_expression_state(n_param=3)
        state, val = expr.apply(state)

        self.assertTupleEqual(val.shape, (3, 1))

        data = val.get_poly(0, 0)
        self.assertDictEqual({
            tuple(): 1.0,
        }, data)

        data = val.get_poly(1, 0)
        self.assertDictEqual({
            ((1, 2),): 1.0,
        }, data)

        data = val.get_poly(2, 0)
        self.assertDictEqual({
            ((0, 1), (1, 1)): 1.0,
        }, data)
