import unittest

from polymatrix.expressionstate.init.initexpressionstate import init_expression_state
import polymatrix.expression.initexpressionbase


class TestQuadraticIn(unittest.TestCase):

    def test_1(self):
        terms = {
            (0, 0): {
                ((0, 1),): 1.0,
            },
            (1, 0): {
                ((1, 1),): 1.0,
            },
            (0, 1): {
                ((1, 1),): 1.0,
                ((2, 1),): 1.0,
            },
        }

        underlying = polymatrix.expression.initexpressionbase.init_from_terms_expr(
            terms=terms,
            shape=(2, 2),
        )

        expr = polymatrix.expression.initexpressionbase.init_symmetric_expr(
            underlying=underlying,
        )

        state = init_expression_state(n_param=2)
        state, val = expr.apply(state)

        data = val.get_poly(0, 0)
        self.assertDictContainsSubset({
            ((0, 1),): 1.0,
        }, data)

        data = val.get_poly(0, 1)
        self.assertDictContainsSubset({
            ((1, 1),): 1.0,
            ((2, 1),): 0.5,
        }, data)

        data = val.get_poly(1, 0)
        self.assertDictContainsSubset({
            ((1, 1),): 1.0,
            ((2, 1),): 0.5,
        }, data)
    