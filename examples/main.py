import sympy
import polymatrix

state = polymatrix.init_expression_state()

x1, x2 = sympy.symbols('x1, x2')
x = polymatrix.from_((x1, x2))

f1 = polymatrix.from_(x1 + x2)
f2 = polymatrix.from_(x1 + x1*x2)

f3 = f1 + f2          

# prints the data structure of the expression
# AdditionExprImpl(left=FromSympyExprImpl(data=((x1 + x2,),)), right=FromSympyExprImpl(data=((x1*x2 + x1,),)))
print(f3)

state, poly_matrix = f3.apply(state)

# prints the data structure of the polynomial matrix
# PolyMatrixImpl(terms={(0, 0): {((0, 1), (1, 1)): 1, ((0, 1),): 2, ((1, 1),): 1}}, shape=(1, 1))
print(poly_matrix)

state, sympy_repr = polymatrix.to_sympy(f3,).apply(state)

# prints the sympy representation of the polynomial matrix
# [[x1*x2 + 2*x1 + x2]]
print(sympy_repr)

state, dense_repr = polymatrix.to_dense((f3,), x).apply(state)

# prints the numpy matrix representations of the polynomial matrix
# array([[2., 1.]])
# array([[0. , 0.5, 0.5, 0. ]])
print(dense_repr.data[0][1])               # numpy array
print(dense_repr.data[0][2].toarray())     # sparse scipy array